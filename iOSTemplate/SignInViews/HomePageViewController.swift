//
//  HomePageViewController.swift
//  Cookey
//
//  Created by Juan Jose on 23/09/22.
//

import UIKit
import Firebase
import FirebaseAuth

@available(iOS 13.0, *)
class HomePageViewController: UIViewController {

    @IBOutlet weak var inventoryButton: UIButton!
    
    @IBOutlet weak var recipesButton: UIButton!
    
    @IBOutlet weak var promosButton: UIButton!
    
    @IBOutlet weak var logOutButton: UIButton!
    
    @IBOutlet weak var contactUsButton: UIButton!
    
    //User Defaults
    let defaults = UserDefaults.standard
    
    //Error label for explaining troubles on the view.
    @IBOutlet weak var logOutLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configureButtons()
    }
    
    func configureButtons(){
        
        self.logOutLabel.text = ""
        
        inventoryButton.layer.cornerRadius = 10
        
        recipesButton.layer.cornerRadius = 10
        
        promosButton.layer.cornerRadius = 10
        
        logOutButton.layer.cornerRadius = 10
        logOutButton.layer.borderWidth = 1
        logOutButton.layer.borderColor = UIColor.black.cgColor
        
        contactUsButton.layer.cornerRadius = 10
        contactUsButton.layer.borderWidth = 1
        contactUsButton.layer.borderColor = UIColor.black.cgColor
        
    }
    
    func transitionToWelcome() {
        
        let viewController = storyboard?.instantiateViewController(identifier: "Welcome") as? ViewController
        
        view.window?.rootViewController = viewController
        view.window?.makeKeyAndVisible()
        
    }
    
    @IBAction func contactUsButtonPressed(_ sender: Any) {
        imprimir("Boton contact us presionado")
        let email = "a.trianaa@uniandes.edu.co"
        if let url = URL(string: "mailto:\(email)") {
            UIApplication.shared.open(url)
        }
    }
    
    @IBAction func logOutTapped(_ sender: Any) {
        
        //Managing eventual connectivity while log out the app
        if !Connectivity.isConnectedToInternet {
            self.logOutLabel.text = "Fail: No Internet"
            DispatchQueue.main.asyncAfter(deadline: .now()+3, execute: {self.logOutLabel.text=""})
        }else{
            self.logOutLabel.text = "Redirecting..."
            
            //Sign out from FireBase
            let auth = Auth.auth()
            do{
                try auth.signOut()
            }catch let signOutError {
                print(signOutError.localizedDescription)
            }
            
            //Putting defaults with a null user value
            defaults.removeObject(forKey: "UID")
            
            DispatchQueue.main.asyncAfter(deadline: .now()+2, execute: {
                //Transition to the welcome page
                self.transitionToWelcome()
                
            })
            
        }
        
    }
    
    func imprimir(_ mensaje:String){
        print("HomePageViewController.swift: " + mensaje)
    }
    
}
