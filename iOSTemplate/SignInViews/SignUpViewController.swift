//
//  SignUpViewController.swift
//  Cookey
//
//  Created by Juan Jose on 4/10/22.
//

import UIKit
import FirebaseAuth
import Firebase
import FirebaseFirestore

@available(iOS 13.0, *)
class SignUpViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var firstNameTextField: UITextField!
    
    @IBOutlet weak var lastNameTextField: UITextField!
    
    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var signUpButton: UIButton!
    
    @IBOutlet weak var errorLabel: UILabel!
    
    @IBOutlet weak var backButton: UIButton!
    
    //User Defaults
    let defaults = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setUpElements()
        
    }
    
    func setUpElements(){
        
        //Configure design button parameters
        configureButtons()
        
        //Hide error label
        errorLabel.alpha = 0
        
        //Text field for first name
        firstNameTextField.delegate = self
        firstNameTextField.tag = 1
        //Text field for last name
        lastNameTextField.delegate = self
        lastNameTextField.tag = 2
        //Text field for email
        emailTextField.delegate = self
        emailTextField.tag = 3
        //Text field for password
        passwordTextField.delegate = self
        passwordTextField.tag = 4
    }
    
    func configureButtons(){
        
        signUpButton.layer.cornerRadius = 10
        signUpButton.layer.borderWidth = 3
        signUpButton.layer.borderColor = UIColor.systemYellow.cgColor
        
        backButton.layer.cornerRadius = 10
        backButton.layer.borderWidth = 3
        backButton.layer.borderColor = UIColor.systemYellow.cgColor
        
    }

    // Put a character limit on the texts fields
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField.tag == emailTextField.tag {
            let currenttxt = textField.text ?? ""
            guard let stringRange = Range(range, in: currenttxt) else {
                return false
            }
            
            let updatetxt = currenttxt.replacingCharacters(in: stringRange, with: string)
            
            return updatetxt.count <= 50
        }else if textField.tag == passwordTextField.tag {
            let currenttxt = textField.text ?? ""
            guard let stringRange = Range(range, in: currenttxt) else {
                return false
            }
            
            let updatetxt = currenttxt.replacingCharacters(in: stringRange, with: string)
            
            return updatetxt.count <= 16
        }else{
            let currenttxt = textField.text ?? ""
            guard let stringRange = Range(range, in: currenttxt) else {
                return false
            }
            
            let updatetxt = currenttxt.replacingCharacters(in: stringRange, with: string)
            
            return updatetxt.count <= 20
        }
        
    }
    
    // Check the fields and validate the the data is correct. If everything is correct, this method returns nil. Otherwise, it returns the error message.
    func validateFields() -> String? {
        
        // Check that all fields are filled in
        if firstNameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" || lastNameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" || emailTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" || passwordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" {
            return "Please fill in all fields!"
        }
        
        // Check the format for the first name and last name fields
        if firstNameTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines).contains("0") == true || firstNameTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines).contains("1") == true || firstNameTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines).contains("2") == true || firstNameTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines).contains("3") == true || firstNameTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines).contains("4") == true || firstNameTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines).contains("5") == true || firstNameTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines).contains("6") == true || firstNameTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines).contains("7") == true || firstNameTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines).contains("8") == true || firstNameTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines).contains("9") {
            return "Please put a valid first name."
        }
        
        if lastNameTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines).contains("0") == true || lastNameTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines).contains("1") == true || lastNameTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines).contains("2") == true || lastNameTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines).contains("3") == true || lastNameTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines).contains("4") == true || lastNameTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines).contains("5") == true || lastNameTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines).contains("6") == true || lastNameTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines).contains("7") == true || lastNameTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines).contains("8") == true || lastNameTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines).contains("9") == true {
            return "Please put a valid last name."
        }
        
        // Check if the password is secure
        let cleanedPassword = passwordTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        
        if Utilities.isPasswordValid(password: cleanedPassword) == false {
            // Password is not secure enough
            return "Please make sure your password is at least 8 characters, contains a special character, a number, a letter and a capital letter."
        }
        
        // Check that the email format is correct
        if emailTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).contains("@") == false {
            return "Please put a valid email format."
        }
        if emailTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).contains(".") == false {
            return "Please put a valid email format."
        }
        if emailTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).contains("@.") == true {
            return "Please put a valid email format."
        }
        
        return nil
    }
    
    func textFieldShouldReturn(_ textField: UITextField)->Bool{
        
        self.view.endEditing(true)
        return false
    }
    
    @IBAction func closeKeyboard(_ sender: Any) {
        emailTextField.endEditing(false)
        passwordTextField.endEditing(false)
        firstNameTextField.endEditing(false)
        lastNameTextField.endEditing(false)
    }
    
    @IBAction func signUpTapped(_ sender: Any) {
        
        // Managing eventual connectivity while registering on the app.
        if !Connectivity.isConnectedToInternet {
            showError(message: "¡No internet connection available!")
        }else{
            //Validate the fields
            let error = validateFields()
            
            if error != nil {
                
                // Something is wrong with the fields, show error message
                showError(message: error!)
            }
            else {
                
                //Create cleaned versions of the data
                let firstName = firstNameTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
                let lastName = lastNameTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
                let email = emailTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
                let password = passwordTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
                
                //Create the user
                Auth.auth().createUser(withEmail: email, password: password) { (result, err) in
                    
                    //Check for errors
                    if err != nil {
                        // There was an error creating the user
                        self.showError(message: "Error creating user, this email is currently used in our database.")
                    }
                    else {
                        
                        // User created successfuly, now store the first name and last name
                        let db = Firestore.firestore()
                        
                        db.collection("user").document(result!.user.uid).setData(["firstName":firstName, "lastName":lastName, "country": "Colombia", email:email]) { (error) in
                            print("Creando nuevo usuario " + firstName + " " + lastName)
                            if error != nil {
                                // Show error message
                                self.showError(message: "Error saving user data")
                            }
                        }
                        //Saving users data
                        self.defaults.set(result!.user.uid, forKey: "UID")
                        
                        //Transition to the home screen
                        self.transitionToHome()
                    }
                    
                }
                
            }
            
        }
        
    }
    
    @IBAction func backTapped(_ sender: Any) {
        //Transition to the welcome page
        transitionToWelcome()
    }
    
    func showError( message:String) {
        errorLabel.text = message
        errorLabel.textColor = UIColor.black
        errorLabel.alpha = 1
    }
    
    func transitionToHome() {
        
        let homePageViewController = storyboard?.instantiateViewController(identifier: "HomePage") as? HomePageViewController
        
        view.window?.rootViewController = homePageViewController
        view.window?.makeKeyAndVisible()
        
    }
    
    func transitionToWelcome() {
        
        let viewController = storyboard?.instantiateViewController(identifier: "Welcome") as? ViewController
        
        view.window?.rootViewController = viewController
        view.window?.makeKeyAndVisible()
        
    }

}
