//
//  ViewController.swift
//  Cookey
//
//  Created by Juan Jose on 4/10/22.
//

import UIKit

class ViewController: UIViewController {
    
    let imprimirMensajes = true;
    
    @IBOutlet weak var loginButton: UIButton!
    
    @IBOutlet weak var registerButton: UIButton!
    
    @IBOutlet weak var myPetButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        setUpElements()
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        transitionToHome()
    }
    
    
    func setUpElements(){

        // Configuring design button parameters
        configureButtons()
    }
    
    func configureButtons(){
        
        loginButton.layer.cornerRadius = 10
        loginButton.layer.borderWidth = 3
        loginButton.layer.borderColor = UIColor.systemYellow.cgColor
        
        registerButton.layer.cornerRadius = 10
        registerButton.layer.borderWidth = 3
        registerButton.layer.borderColor = UIColor.systemYellow.cgColor
        
    }
    
    func transitionToHome() {
        
        if UserDefaults.standard.value(forKey: "UID") != nil {
            if #available(iOS 13.0, *) {
                let homePageViewController = storyboard?.instantiateViewController(identifier: "HomePage") as? HomePageViewController
                view.window?.rootViewController = homePageViewController
                view.window?.makeKeyAndVisible()
                self.imprimir("UID encontrado, rediriendo a HomePage")
            } else {
                // Fallback on earlier versions
            }
            
        }
    }
    
    func imprimir(_ mensaje:String){
        print("ViewController.swift: " + mensaje)
    }

}
