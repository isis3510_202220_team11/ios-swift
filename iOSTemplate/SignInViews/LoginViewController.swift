//
//  LoginViewController.swift
//  Cookey
//
//  Created by Juan Jose on 4/10/22.
//

import UIKit
import FirebaseAuth

@available(iOS 13.0, *)
class LoginViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var signInButton: UIButton!
    
    @IBOutlet weak var errorLabel: UILabel!
    
    @IBOutlet weak var backButton: UIButton!
    
    //User Defaults
    let defaults = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setUpElements()
        
    }
    
    func setUpElements(){
        
        //Configure design button parameters
        configureButtons()
        
        //Hide the error label
        errorLabel.alpha = 0
        
        //Text field for email
        emailTextField.delegate = self
        emailTextField.tag = 1
        //Text field for password
        passwordTextField.delegate = self
        passwordTextField.tag = 2
        
    }
    
    func configureButtons(){
        
        signInButton.layer.cornerRadius = 10
        signInButton.layer.borderWidth = 3
        signInButton.layer.borderColor = UIColor.systemYellow.cgColor
        
        backButton.layer.cornerRadius = 10
        backButton.layer.borderWidth = 3
        backButton.layer.borderColor = UIColor.systemYellow.cgColor
        
    }

    // Put a character limit on the texts fields
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField.tag == emailTextField.tag {
            let currenttxt = textField.text ?? ""
            guard let stringRange = Range(range, in: currenttxt) else {
                return false
            }
            
            let updatetxt = currenttxt.replacingCharacters(in: stringRange, with: string)
            
            return updatetxt.count <= 50
        }else{
            let currenttxt = textField.text ?? ""
            guard let stringRange = Range(range, in: currenttxt) else {
                return false
            }
            
            let updatetxt = currenttxt.replacingCharacters(in: stringRange, with: string)
            
            return updatetxt.count <= 16
        }
        
    }
    
    func validateFields() -> String? {
        
        // Check that all fields are filled in
        if emailTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" || passwordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" {
            return "Please fill in all fields."
        }
        
        // Check that the email format is correct
        if emailTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).contains("@") == false {
            return "Please put a valid email format."
        }
        if emailTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).contains(".") == false {
            return "Please put a valid email format."
        }
        if emailTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).contains("@.") == true {
            return "Please put a valid email format."
        }
        
        return nil
    }
    
    @IBAction func signInTapped(_ sender: Any) {
        
        // Managing eventual cnnectivity while login on the app
        if !Connectivity.isConnectedToInternet {
            self.showError(message: "¡No internet connection available!")
        }else{
            // Validate the text fields
            let error = validateFields()
            
            if error != nil {
                
                // Something is wrong with the fields, show error message
                showError(message: error!)
            }
            else {
                
                // Create cleaned versions of the text fields
                let email = emailTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
                let password = passwordTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
                
                // Signing in the user
                Auth.auth().signIn(withEmail: email, password: password) { (result, err) in
                    
                    // Check for errors
                    if err != nil {
                        // Could not sign in
                        self.showError(message: "Wrong email or password!")
                    }
                    else {
                        //Saving users data
                        self.defaults.set(result!.user.uid, forKey: "UID")
                        
                        //Transition to the home screen
                        self.transitionToHome()
                    }
                }
                
            }
            
        }
        
        
    }
    
    @IBAction func backTapped(_ sender: Any) {
        //Transition to the welcome page
        transitionToWelcome()
    }
    
    func showError( message:String) {
        errorLabel.text = message
        errorLabel.textColor = UIColor.black
        errorLabel.alpha = 1
    }
    
    func transitionToHome() {
        
        let homePageViewController = storyboard?.instantiateViewController(identifier: "HomePage") as? HomePageViewController
        
        view.window?.rootViewController = homePageViewController
        view.window?.makeKeyAndVisible()
        
    }
    
    func transitionToWelcome() {
        
        let viewController = storyboard?.instantiateViewController(identifier: "Welcome") as? ViewController
        
        view.window?.rootViewController = viewController
        view.window?.makeKeyAndVisible()
        
    }
    
    @IBAction func closeKeyboard(_ sender: Any) {
        emailTextField.endEditing(false)
        passwordTextField.endEditing(false)
    }
    
}
