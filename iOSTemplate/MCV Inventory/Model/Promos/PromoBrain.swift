//
//  PromoBrain.swift
//  Cookey
//
//  Created by Juan Jose on 5/11/22.
//

import Foundation
import Firebase

class PromoBrain
{
    //Lista de promociones disponibles
    var lista = [Promo]()
    
    //Cargar las promociones que estan en la base de datos
    func loadAllPromos(semaphore:DispatchSemaphore)
    {
        //Base de datos
        let db = Firestore.firestore()
        
        //Tomar las promociones en la base de datos
        db.collection("promos").getDocuments
        { snapshot, error in
            // Verificacion de errores
            if error == nil
            {
                // No hay errores
                // Proceder con normalidad
                if let snapshot = snapshot
                {
                    // Obtener promociones
                    self.lista = snapshot.documents.map
                    {doc in
                        return Promo(
                            id: doc.documentID,
                            name: doc["name"] as? String ?? "",
                            after: doc["after"] as? String ?? "",
                            before: doc["before"] as? String ?? "",
                            discount: doc["discount"] as? Int ?? 0,
                            place: doc["place"] as? String ?? "",
                            description: doc["description"] as? String ?? "",
                            img: doc["img"] as? String ?? "")}
                }
                
                // Se cargan las primeras 6 promociones en local para que tengan acceso cuando no haya señal
                self.saveLocal()
                
            }
            else
            {
                // Manejar el error
                print("---------------------------------------")
                print("Error al intentar cargas la promociones")
                print("---------------------------------------")
            }
            
            for promo in self.lista
            {
                print(promo.name)
            }
            semaphore.signal()
            
        }
        
    }
    
    //Carga las primeras 6 promociones de la lista en local
    func saveLocal()
    {
        let seisPrimeros = Array(lista.prefix(6))
        let listaGuardada = exportLocal()
        
        
        // Si son iguales no hace nada
        if (seisPrimeros == listaGuardada)
        {
        }
        
        // Si no son iguales, agrega a local los nuevos elementos
        else
        {
            if let data = try? PropertyListEncoder().encode(lista) {
                UserDefaults.standard.set(data, forKey: UserDefaultsKeys().getPromoKey())
            }
        }
    }
    
    //Pasa los datos guardados a una lista
    func exportLocal() -> [Promo]
    {
        if let promocionesLocally = UserDefaults.standard.data(forKey: UserDefaultsKeys().getPromoKey()) {
            if let elementos = try?  PropertyListDecoder().decode([Promo].self, from: promocionesLocally)
            {
                return elementos
            }
        }
        return []
    }
    
    /**
        Asigna a todas los valores locales
     */
    func loadLocal()
    {
        lista = exportLocal()
    }
    
    /**
     Retorna una lista con las recetas guardadas en la variable promos".
     */
    func getPromos() -> [Promo]
    {
        return lista
    }
    
}
