//
//  Promo.swift
//  Cookey
//
//  Created by Juan Jose on 5/11/22.
//

import Foundation

class Promo: Codable, Equatable {
    static func == (lhs: Promo, rhs: Promo) -> Bool {
        return lhs.id == rhs.id
    }
    
    //Atributos que tiene una promocion
    var id: String
    var name: String
    var after: String
    var before: String
    var discount: Int
    var place : String
    var description: String
    var img: String
    
    init(id: String, name: String, after: String, before: String, discount: Int, place: String, description: String, img: String) {
        self.id = id
        self.name = name
        self.after = after
        self.before = before
        self.discount = discount
        self.place = place
        self.description = description
        self.img = img
    }
    
}
