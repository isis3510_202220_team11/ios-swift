//
//  Product.swift
//  Cookey
//
//  Created by Santiago Triana on 1/11/22.
//

import Foundation

class InventoryProduct: Codable
{
    // Elementos que componen un producto
    // Se basa en el JSON de los productos
    var id: String
    var quantity: Int
    var name: String
    var unit: String
    var image: String
    
    // Inicializador de un producto
    init(id: String, quantity: Int, name: String, unit: String, image:String) {
        self.id = id
        self.quantity = quantity
        self.name = name
        self.unit = unit
        self.image = image
        
    }
}
