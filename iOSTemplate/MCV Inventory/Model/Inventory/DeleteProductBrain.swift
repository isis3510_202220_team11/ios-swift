//
//  InventoryBrain.swift
//  Cookey
//
//  Created by Santiago Triana on 1/11/22.
//  Clase que funciona como el modelo del delete product

import Foundation
import Firebase

class DeleteProductBrain
{
    let imprimirMensajes = true
    var success = false

    /**
     Elimina el producto pasado por parametro ("product") en la base de datos
     */
    func deleteProduct(semaphore:DispatchSemaphore, product:InventoryProduct)
    {
        imprimir("Cargando productos...")
        // Referencia a la base de datos
        let db = Firestore.firestore()
        
        //Tomar las recetas de la base de datos
        
        db.collection("user").document((UserDefaults.standard.string(forKey: UserDefaultsKeys().getUserIdKey())) ?? "").collection("inventory").document(product.id).delete()
        { err in
            if let err = err{
                self.imprimir("Error borrando el producto: \(err)")
                self.success = false
            }else{
                self.imprimir("Producto eliminado correctamente")
                self.success = true
            }
            semaphore.signal()
        }
        
    }

    
    func imprimir(_ mensaje:String){if imprimirMensajes {print("DeleteProductBrain: "+mensaje)}}
    
}
