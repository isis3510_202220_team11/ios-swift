//
//  InventoryBrain.swift
//  Cookey
//
//  Created by Santiago Triana on 1/11/22.
//  Clase que funciona como el modelo del inventario

import Foundation
import Firebase

class InventoryBrain
{
    
    /**
     Lista con el inventario del usuario
     */
    var productos = [InventoryProduct]()
    
    let imprimirMensajes = true
    
    
    /**
     Carga de la base de datos los productos del usuario
     */
    func loadAllProducts(semaphore:DispatchSemaphore)
    {
        imprimir("Cargando productos...")
        // Referencia a la base de datos
        let db = Firestore.firestore()
        
        //Tomar las recetas de la base de datos
        
        print((UserDefaults.standard.string(forKey: UserDefaultsKeys().getUserIdKey())) ?? "")
        print(UserDefaultsKeys().getInventoryKey())
        print(UserDefaultsKeys().getUserIdKey())
        
        db.collection("user").document((UserDefaults.standard.string(forKey: UserDefaultsKeys().getUserIdKey())) ?? "").collection("inventory").getDocuments
        { snapshot, error in
            // Verificacion de errores
            if error == nil
            {
                // No error
                // Procede con normalidad
                if let snapshot = snapshot
                {
                    
                    // Toma todo el inventario del usuario recetas
                    self.productos = snapshot.documents.map
                    {doc in
                        return InventoryProduct(
                            id: doc.documentID,
                            quantity: doc["quantity"] as? Int ?? 0,
                            name: doc["name"] as? String ?? "",
                            unit: doc["unit"] as? String ?? "",
                            image: doc["image"] as? String ?? "")}
                }
                
                var mensaje:String = "Productos descargados correctamente: "+String(self.productos.count)
                self.imprimir(mensaje)
                
                
                //En caso que logre hacer fetch del inventario lo guarda en local
                self.saveProductsLocally()
                
                mensaje = "Productos guardados en disco correctamente: "+String(self.getProductsLocally().count)
                self.imprimir(mensaje)
                
            }
            else
            {
                // Manejar el error que salga
                self.imprimir("-----------------------------")
                self.imprimir("Error en la carga de inventario")
                //En caso que haya fallado la carga del inventario hace un recover de los productos guardados en local
                self.setProductsToLocal()
                var mensaje:String = "Haciendo recover de productos de disco. Productos recuperados: "+String(self.productos.count)
                self.imprimir(mensaje)
                self.imprimir("-----------------------------")
            }
            
            semaphore.signal()
            
            
            
            //
        }
        
    }
    /**
     Guarda los elementos de la lista products en los userDefaults.
     */
    func saveProductsLocally(){
        //        let defaults = UserDefaults.standard
        //        defaults.set(productos, forKey:"inventory")
        //
        if let data = try? PropertyListEncoder().encode(productos) {
            UserDefaults.standard.set(data, forKey: UserDefaultsKeys().getInventoryKey())
        }
    }
    
    /**
     Retorna una lista con los InventoryProducts guardados en UserDefaults, o una lista vacía en caso de no existir la llave "inventory".
     */
    func getProductsLocally() -> [InventoryProduct]{
        //        let defaults = UserDefaults.standard
        //        let savedArray = defaults.object(forKey: "SavedArray") as? [InventoryProduct] ?? [InventoryProduct]()
        
        if let productosLocally = UserDefaults.standard.data(forKey: UserDefaultsKeys().getInventoryKey()) {
            if let elementos = try?  PropertyListDecoder().decode([InventoryProduct].self, from: productosLocally) {
//                for producto in elementos
//                {
//                    self.imprimir(producto.name)
//                    self.imprimir(String(producto.quantity))
//                    self.imprimir(producto.unit)
//                    self.imprimir(producto.image)
//                }
                return elementos
            }
            
        }
        //A esta linea de codigo nunca debería llegar, sino cuando no existe la llave "inventory" en los userDefaults
        self.imprimir("No se han encontrado productos en el userDefault")
        return []
    }
    
    /**
     Establece la variable products con los InventoryProducts de la clase, a los elementos guardados en los userDefaults.
     */
    func setProductsToLocal(){
        productos = getProductsLocally()
    }
    
    /**
     Retorna una lista con los InventoryProducts guardados en la variable productos".
     */
    func getProducts() -> [InventoryProduct]{
        //        let defaults = UserDefaults.standard
        //        let savedArray = defaults.object(forKey: "SavedArray") as? [InventoryProduct] ?? [InventoryProduct]()
        
        return productos
    }
    
    func imprimir(_ mensaje:String){if imprimirMensajes {print("InventoryBrain: "+mensaje)}}
    
}
