//
//  IngredientRecipe.swift
//  Cookey
//
//  Created by Juan Sebastian Ramirez on 6/10/22.
//  Esta clase hace referencia a los ingredientes que requiere una receta
//  Esta basado en la conexion de la bd entre recetas e ingredientes

import Foundation

class IngredientRecipe: Ingredient
{
    var idReal: String
    var idIng: String
    var idRec: String
    var cantidad: Int
    
    // Inicializador de la clase
    init(id: String, unit: String, name: String, calories: Int, idReal: String, idIng: String, idRec: String, cantidad:Int, fechaExpiracion: Date)
    {
        self.idReal = idReal
        self.idIng = idIng
        self.idRec = idRec
        self.cantidad = cantidad
        super.init(id: id, unit: unit, name: name, calories: calories)
    }
}
