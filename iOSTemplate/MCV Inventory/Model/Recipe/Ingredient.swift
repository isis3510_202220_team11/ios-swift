//
//  Ingredient.swift
//  Cookey
//
//  Created by Juan Sebastian Ramirez on 6/10/22.
//  Super clase para todos los ingredientes que se van a tener

import Foundation

class Ingredient
{
    // Elementos de la super clase ingrediente
    var id: String
    var unit: String
    var name: String
    var calories: Int
    
    // Inicializador de la clase
    init(id: String, unit: String, name: String, calories: Int)
    {
        self.id = id
        self.unit = unit
        self.name = name
        self.calories = calories
    }
    
}
