//
//  RecipeBrain.swift
//  Cookey
//
//  Created by Juan Sebastian Ramirez on 6/10/22.
//  Clase que representa el modelo que se encarga de las recetas

import Foundation
import Firebase

class RecipeBrain
{
    //Lista con todas las recetas
    var todas = [Recipe]()
    
    
    /**
     Carga todas las recetas de la base de datos
     Utiliza un semaforo para avisar cuando ya estén cargadas
     */
    func loadAllRecipes(semaphore:DispatchSemaphore)
    {
        // Referencia a la base de datos
        let db = Firestore.firestore()
        
        //Tomar las recetas de la base de datos
        db.collection("Recipes").getDocuments
        { snapshot, error in
            // Verificacion de errores
            if error == nil
            {
                // No errorres
                // Proceder con normalidad
                if let snapshot = snapshot
                {
                    // Tomar todas las recetas
                    self.todas = snapshot.documents.map
                    {doc in
                        return Recipe(
                            id: doc.documentID,
                            calories: doc["calories"] as? Int ?? 0,
                            country: doc["country"] as? String ?? "",
                            img: doc["img"] as? String ?? "",
                            likeBy: doc["likedBy"] as? Int ?? 0,
                            name: doc["name"] as? String ?? "",
                            notes: doc["recipe"] as? String ?? "",
                            type: doc["type"] as? String ?? "")}
                }
                
                // Se cargan las primeras 6 recetas en local para que tengan acceso cuando no haya señal
                self.saveLocal()
            }
            
            else
            {
                // Manejar el error que salga
                print("-----------------------------")
                print("Error en la carga de recetas")
                print("-----------------------------")
            }
            
            for recipe in self.todas
            {
                print(recipe.name)
            }
            semaphore.signal()
        }
        
    }
    
    //Carga las primeras 6 recetas de la lista en local
    func saveLocal()
    {
        let seisPrimeros = Array(todas.prefix(6))
        let listaGuardada = exportFromLocal()
        
        
        // Si son iguales no hace nada
        if (seisPrimeros == listaGuardada)
        {
        }
        
        // Si no son iguales, agrega a local los nuevos elementos
        else
        {
            if let data = try? PropertyListEncoder().encode(todas) {
                UserDefaults.standard.set(data, forKey: UserDefaultsKeys().getRecipeKey())
            }
        }
    }
    
    //Pasa los datos guardados a una lista
    func exportFromLocal() -> [Recipe]
    {
        if let productosLocally = UserDefaults.standard.data(forKey: UserDefaultsKeys().getRecipeKey()) {
            if let elementos = try?  PropertyListDecoder().decode([Recipe].self, from: productosLocally)
            {
                return elementos
            }
        }
        return []
    }
    
    /**
        Asigna a todas los valores locales
     */
    func loadLocal()
    {
        todas = exportFromLocal()
    }
    
    
    // Funcion para buscar recetas por calorias
    func loadByCalories() -> [Recipe]
    {
        var respuesta = todas
        
        respuesta.sort
        {
            $0.calories < $1.calories
        }
        
        return respuesta
    }
    
    
    // Funcion para buscar recetas por pais
    func loadByCountry(country: String) -> [Recipe]
    {
        var respuesta = [Recipe]()
        
        for recipe in todas
        {
            if recipe.country == country
            {
                respuesta.append(recipe)
            }
        }
        
        return respuesta
    }
    
    /**
        Funcion que selecciona las recetas de calorias menores al parametro
     */
    func getRecipesUnderCal( calories: Int) -> [Recipe]
    {
        var respuesta = [Recipe]()
        
        for recipe in todas
        {
            if recipe.calories <= calories
            {
                respuesta.append(recipe)
            }
        }
        
        respuesta.sort
        {
            $1.calories < $0.calories
        }
        
        return respuesta
    }
    
    /**
     Retorna una lista con las recetas guardadas en la variable recipes".
     */
    func getRecipes() -> [Recipe]
    {
        return todas
    }
}

