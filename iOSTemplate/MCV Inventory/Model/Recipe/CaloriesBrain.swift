//
//  CaloriesBrain.swift
//  Cookey
//
//  Created by JUAN C on 30/11/22.
//

import Foundation

class CaloriesBrain
{
    //Calorias actuales
    var calories: Int = 0
    
    //Guarda calorias en local
    func saveLocal(cals: Int)
    {
        // Si es el mismo valor no se hace nada
        if (cals == calories)
        {
        }
        
        // Si no son iguales, agrega a local los nuevos elementos
        else
        {
            calories = cals
            UserDefaults.standard.set(calories, forKey: UserDefaultsKeys().getCaloriesKey())
            UserDefaults.standard.synchronize()
        }
    }
    
    //Carga calorias desde local
    func getFromLocal() -> Int
    {
        print("------ANTES--------")
        var cals = UserDefaults.standard.integer(forKey: UserDefaultsKeys().getCaloriesKey())
        calories = cals
        return calories
    }
}
