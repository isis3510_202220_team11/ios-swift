//
//  Recipe.swift
//  Cookey
//
//  Created by Juan Sebastian Ramirez on 6/10/22.
//  Clase que representa una receta en la vida real

import Foundation

class Recipe: Codable, Equatable
{
    static func == (lhs: Recipe, rhs: Recipe) -> Bool {
        return lhs.id == rhs.id
    }
    
    // Elementos que componen las recetas
    // Se basa en el JSON de recetas
    var id: String
    var calories: Int
    var country: String
    var img: String
    var likeBy: Int
    var name: String
    var notes: String
    var type: String
    
    // Inicializador de una receta
    init(id: String, calories: Int, country: String, img: String, likeBy: Int, name: String, notes: String, type: String) {
        self.id = id
        self.calories = calories
        self.country = country
        self.img = img
        self.likeBy = likeBy
        self.name = name
        self.notes = notes
        self.type = type
    }
}
