//
//  IngredientUser.swift
//  Cookey
//
//  Created by Juan Sebastian Ramirez on 6/10/22.
//  Representa un ingrediente que tiene un usuario en este momento en su casa
//  Hereda de Ingrediente

import Foundation

class IngredientUser: Ingredient
{
    // Variables adicionales, se convierte en una tabla de conexión
    var idReal: String
    var idIng: String
    var idUsr: String
    var cantidad: Int
    var fechaExpiracion: Date
    
    // Inicializador de la clase
    init(id: String, unit: String, name: String, calories: Int, idReal: String, idIng: String, idUsr: String, cantidad:Int, fechaExpiracion: Date)
    {
        self.idReal = idReal
        self.idIng = idIng
        self.idUsr = idUsr
        self.cantidad = cantidad
        self.fechaExpiracion = fechaExpiracion
        super.init(id: id, unit: unit, name: name, calories: calories)
    }
}
