//
//  CaloriesViewController.swift
//  Cookey
//
//  Created by Juan Sebastian Ramirez on 8/10/22.
//

import UIKit
import SkeletonView
import PINRemoteImage

var data : [Recipe] = []

class CaloriesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, SkeletonTableViewDataSource
{

    @IBOutlet weak var caloriesTable: UITableView!
        
    //Model
    var model = RecipeBrain()
    
    //Colores para los esqueletos de carga
    let colorMorado = UIColor(red: 75/255, green: 51/255, blue: 252/255, alpha: 1)
    let colorGris = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.5)
        
    override func viewDidLoad()
    {
        super.viewDidLoad()
        //let semaphore = DispatchSemaphore(value: 1)
        //model.loadAllRecipes(semaphore: semaphore)
        
        self.caloriesTable.dataSource = self
        self.caloriesTable.delegate = self
        data = self.model.loadByCalories()
        self.caloriesTable.reloadData()
        
        //Carga del esqueleto cuando no han llegado los datos
        if #available(iOS 13.0, *) {
            caloriesTable.showAnimatedGradientSkeleton(usingGradient: .init(baseColor: colorMorado), animation: nil, transition: .crossDissolve(0.25))
        } else {
            // Fallback on earlier versions
        }
        
        
    }
        
    
   func tableView(_ caloriesTable: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return data.count
    }
    
    func tableView(_ caloriesTable: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let recipe = data[indexPath.row]
        let cell = caloriesTable.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! RecipeCaloriesTableViewCell
        cell.recipeName.text = recipe.name
        cell.recipeType.text = recipe.type
        cell.recipeCalories.text = String(recipe.calories)
        cell.loadImageFromUrl(URLAddress: recipe.img)
        return cell
    }
    
    func tableView(_ caloriesTable: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 180
    }
    
    func collectionSkeletonView(_ skeletonView: UITableView, cellIdentifierForRowAt indexPath: IndexPath) -> ReusableCellIdentifier {
        return RecipeCaloriesTableViewCell.identifier
    }
    
    func loadImageFromUrl(URLAddress: String, productImage:UIImageView) {
        productImage.pin_updateWithProgress = true
        productImage.pin_setImage(from: URL(string: URLAddress)!)
    }
    
    }
