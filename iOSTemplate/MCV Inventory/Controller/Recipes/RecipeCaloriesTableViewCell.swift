//
//  RecipeCaloriesTableViewCell.swift
//  Cookey
//
//  Created by Juan Sebastian Ramirez on 8/10/22.
//

import UIKit

class RecipeCaloriesTableViewCell: UITableViewCell {

    static let identifier = "RecipeCaloriesViewCell"
    
    @IBOutlet weak var recipeImage: UIImageView!
    
    @IBOutlet weak var recipeName: UILabel!
    
    @IBOutlet weak var recipeCalories: UILabel!
    
    @IBOutlet weak var recipeType: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func loadImageFromUrl(URLAddress: String) {
        recipeImage.pin_updateWithProgress = true
        recipeImage.pin_setImage(from: URL(string: URLAddress)!)
    }
}
