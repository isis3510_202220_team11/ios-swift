//
//  RecipeCollectionViewCell.swift
//  Cookey
//
//  Created by Juan Sebastian Ramirez on 6/10/22.
//  Esta clase se encarga de manejar los paneles que van a salir en el view de recetas

import UIKit

class RecipeCollectionViewCell: UICollectionViewCell
{
    // Guarda las imagenes de la comida
    @IBOutlet weak var recipeImage: UIImageView!
    // Guarda el nombre de la comida
    @IBOutlet weak var recipeLabel: UILabel!
    // Guarda la receta de la comida
    var recipeFood: Recipe!
    // Boton de la receta
    @IBOutlet weak var recipeButton: UIButton!
    
    // Va a llenar la cell con los elementos que quiero
    func setup(with recipe: Recipe)
    {
        self.loadImageFromUrl(URLAddress: recipe.img, recipeImage: self.recipeImage)
        recipeLabel.text = recipe.name
        recipeFood = recipe
    }
    
    func loadImageFromUrl(URLAddress: String, recipeImage:UIImageView) {
        recipeImage.pin_updateWithProgress = true
        recipeImage.pin_setImage(from: URL(string: URLAddress)!)
    }
}

extension UIImageView
{
    
    func load(urlString: String)
    {
        guard let url = URL(string: urlString) else {
            return
        }
        DispatchQueue.global().async
        {
            [weak self] in
            if let data = try? Data(contentsOf: url)
            {
                if let image = UIImage(data: data)
                {
                    DispatchQueue.main.async
                    {
                        self?.image = image
                    }
                }
            }
        }
    }
}


