//
//  RecommendationsViewController.swift
//  Cookey
//
//  Created by Juan Sebastian Ramirez on 30/10/22.
//

import UIKit

class RecommendationsViewController: UIViewController {

    //Model
    var model = RecipeBrain()
    
    //Botones para corregirles la forma
    @IBOutlet weak var lowerCalButton: UIButton!
    
    @IBOutlet weak var byCountryButton: UIButton!
    
    @IBOutlet weak var setCalButton: UIButton!
    
    
    @IBAction func lowerCalPressed(_ sender: UIButton)
    {
        performSegue(withIdentifier: "LowerCal", sender: self)
    }
    
    @IBAction func setCalPressed(_ sender: UIButton) {
        performSegue(withIdentifier: "SelectCal", sender: self)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lowerCalButton.layer.cornerRadius = 10
        byCountryButton.layer.cornerRadius = 10
        setCalButton.layer.cornerRadius = 10
        
        // Do any additional setup after loading the view.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "LowerCal"
        {
            let vc = segue.destination as! CaloriesViewController
            vc.model = self.model
        }
        else if segue.identifier == "ByCountry"
        {
            _ = segue.destination as! CountryViewController
        }
        else if segue.identifier == "SelectCal"
        {
            let vc = segue.destination as! SetCaloriesViewController
            vc.model = self.model
        }
            
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
