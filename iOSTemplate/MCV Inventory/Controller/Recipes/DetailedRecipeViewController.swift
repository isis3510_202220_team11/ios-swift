//
//  DetailedRecipeViewController.swift
//  Cookey
//
//  Created by Juan Sebastian Ramirez on 7/10/22.
//  Clase para mostrar las recetas detalladas

import UIKit

let recipeNotificationKey = "co.cooKey.recipe"

class DetailedRecipeViewController: UIViewController
{
    //Receta que se va a mostrar
    var recipe: Recipe?
    
    //Nombre de la receta
    @IBOutlet weak var recipeName: UILabel!
    
    //Imagen de la receta
    @IBOutlet weak var recipeImg: UIImageView!
    
    //Likes de la receta
    @IBOutlet weak var recipeLikes: UILabel!
    
    //Calories de la receta
    @IBOutlet weak var recipeCalories: UILabel!
    
    //Pais de la receta
    @IBOutlet weak var recipeCountry: UILabel!
    
    //Tipo de receta
    @IBOutlet weak var recipeType: UILabel!
    
    //Descripcion detallada de la receta
    @IBOutlet weak var recipeNotes: UILabel!
    
    //Key de la notificacion de la receta
    let recipeKey = Notification.Name(rawValue: recipeNotificationKey)
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        recipeName.text = String(recipe?.name ?? "")
        recipeImg.load(urlString: recipe?.img ?? "")
        recipeLikes.text = String(recipe?.likeBy ?? 0)
        recipeCalories.text = String(recipe?.calories ?? 0)
        recipeCountry.text = recipe?.country
        recipeType.text = recipe?.type
        recipeNotes.text = recipe?.notes

        print("el detailed es: "+selectedRecipe.name)
        // Do any additional setup after loading the view.
        createObservers()
    }
    
    func createObservers() {
        // Receta
        NotificationCenter.default.addObserver(self, selector: #selector(DetailedRecipeViewController.updateRecipeName(notification:)), name: recipeKey, object: selectedRecipe)
        
        NotificationCenter.default.addObserver(self, selector: #selector(DetailedRecipeViewController.updateRecipeImage(notification:)), name: recipeKey, object: selectedRecipe)
        
        NotificationCenter.default.addObserver(self, selector: #selector(DetailedRecipeViewController.updateRecipeLikes(notification:)), name: recipeKey, object: selectedRecipe)
        
        NotificationCenter.default.addObserver(self, selector: #selector(DetailedRecipeViewController.updateRecipeCalories(notification:)), name: recipeKey, object: selectedRecipe)
        
        NotificationCenter.default.addObserver(self, selector: #selector(DetailedRecipeViewController.updateRecipeCountry(notification:)), name: recipeKey, object: selectedRecipe)
        
        NotificationCenter.default.addObserver(self, selector: #selector(DetailedRecipeViewController.updateRecipeType(notification:)), name: recipeKey, object: selectedRecipe)
        
        NotificationCenter.default.addObserver(self, selector: #selector(DetailedRecipeViewController.updateRecipeRecipe(notification:)), name: recipeKey, object: selectedRecipe)
    }
    
    // Funciones para cambiar los elementos por medio del observer
    @objc func updateRecipeName(notification: NSNotification) {
        
        recipeName.text = selectedRecipe.name
        
    }
    
    @objc func updateRecipeImage(notification: NSNotification) {
        
        recipeImg.load(urlString: selectedRecipe.img)
        
    }
    
    @objc func updateRecipeLikes(notification: NSNotification) {
        
        recipeLikes.text = String(selectedRecipe.likeBy)
        
    }
    
    @objc func updateRecipeCalories(notification: NSNotification) {
        
        recipeCalories.text = String(selectedRecipe.calories)
        
    }
    
    @objc func updateRecipeCountry(notification: NSNotification) {
        
        recipeCountry.text = selectedRecipe.country
        
    }
    
    @objc func updateRecipeType(notification: NSNotification) {
        
        recipeType.text = selectedRecipe.type
        
    }
    
    @objc func updateRecipeRecipe(notification: NSNotification) {
        
        recipeNotes.text = selectedRecipe.notes
        
    }
    
}
