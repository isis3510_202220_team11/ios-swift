//
//  CountryViewController.swift
//  Cookey
//
//  Created by Juan Jose on 8/10/22.
//

import UIKit
import MapKit
import CoreLocation

class CountryViewController: UIViewController, UITextFieldDelegate, CLLocationManagerDelegate {
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var countryRecipeImage: UIImageView!
    
    @IBOutlet weak var CountryRecipeLikes: UILabel!
    
    @IBOutlet weak var coutryRecipeCalories: UILabel!
    
    @IBOutlet weak var countryRecipeCountry: UILabel!
    
    @IBOutlet weak var countryRecipeType: UILabel!
    
    @IBOutlet weak var countryRecipeNotes: UILabel!
    
    @IBOutlet weak var checkButton: UIButton!
    
    @IBOutlet weak var errorLabel: UILabel!
    
    //Using the location manager
    let locationManager = CLLocationManager()
    
    //Variables for saving users country and city
    var pais = "Nada"
    var ciudad = "Nadita"
    
    //Variables for saving latitude and longitude
    var lat = 0.0
    var long = 0.0
    
    let model = RecipeBrain()
    
    var count = 0...9
    
    var data: [Recipe] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureButtons()
        errorLabel.text = ""
        
        let semaphore = DispatchSemaphore(value: 1)
        model.loadAllRecipes(semaphore: semaphore)
        
        locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }

    }
    
    func configureButtons(){
        checkButton.layer.cornerRadius = 10
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        lat = locValue.latitude
        long = locValue.longitude
        let location1 = CLLocation(latitude: lat, longitude: long)
        fetchCityAndCountry(from: location1) { city, country, error in
            guard let city = city, let country = country, error == nil else { return }
            print(city + ", " + country)
            self.pais = country
            self.ciudad = city
        }
    }
    
    func fetchCityAndCountry(from location: CLLocation, completion: @escaping (_ city: String?, _ country:  String?, _ error: Error?) -> ()) {
        CLGeocoder().reverseGeocodeLocation(location) { placemarks, error in
            completion(placemarks?.first?.locality,
                       placemarks?.first?.country,
                       error)
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func checkTapped(_ sender: Any) {
        
        errorLabel.text = "Searching..."
        
        if Connectivity.isConnectedToInternet {
            // Get the current location permissions
            let status = CLLocationManager.authorizationStatus()
            var mayorlike = 0
            var recetaPais = model.todas[0]
            
            // Handle each case of location permissions
            if status == .authorizedAlways || status == .authorizedWhenInUse {
                let location2 = CLLocation(latitude: lat, longitude: long)
                fetchCityAndCountry(from: location2) { city, country, error in
                    guard let city = city, let country = country, error == nil else { return }
                    print(city + ", " + country)
                    self.pais = country
                    self.ciudad = city
                }
                errorLabel.text = "Country found correctly!"
                
                for number in count {
                    if(pais.trimmingCharacters(in: .whitespacesAndNewlines) == model.todas[number].country.trimmingCharacters(in: .whitespacesAndNewlines)) {
                        
                        if model.todas[number].likeBy >= mayorlike {
                            mayorlike = model.todas[number].likeBy
                            recetaPais = model.todas[number]
                        }
                        
                    }
                    
                }
                
                countryRecipeImage.load(urlString: recetaPais.img)
                CountryRecipeLikes.text = String(recetaPais.likeBy)
                coutryRecipeCalories.text = String(recetaPais.calories)
                countryRecipeCountry.text = recetaPais.name
                titleLabel.text = recetaPais.country
                countryRecipeType.text = recetaPais.type
                countryRecipeNotes.text = recetaPais.notes
                
                DispatchQueue.main.asyncAfter(deadline: .now()+3, execute: {self.errorLabel.text=""})
            }else{
                print("No se pudo manejar la ubicacion")
                errorLabel.text = "You need to allow us on settings to search for your current location for using this section."
            }
        }else{
            errorLabel.text = "No internet connection for loading the recipe content."
        }
        
    }
    
}
