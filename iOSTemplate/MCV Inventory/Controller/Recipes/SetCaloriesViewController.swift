//
//  SetCaloriesViewController.swift
//  Cookey
//
//  Created by JUAN C on 30/11/22.
//

import UIKit
import SkeletonView
import PINRemoteImage

class SetCaloriesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, SkeletonTableViewDataSource
{
    

    //Elementos del UI
    @IBOutlet weak var numCalLabel: UILabel!
    
    //Boton de carga
    @IBOutlet weak var loadButton: UIButton!
    
    //Boton de cuardado
    @IBOutlet weak var saveButton: UIButton!
    
    //Slider
    @IBOutlet weak var slider: UISlider!
    
    
    //Tabla en la que se van a desplegar todas las recetas con valores menores de calorias a los del input
    @IBOutlet weak var recipesTable: UITableView!
    
    
    // Cuando el slider cambia de valor
    @IBAction func sliderChanged(_ sender: UISlider) {
        numCalLabel.text = String(Int(sender.value))
        
        //Elegir las recetas
        
        data = model.getRecipesUnderCal(calories: Int(sender.value))
        self.recipesTable.reloadData()
    }
    
    
    // Modelo de recipes
    var model = RecipeBrain()
    
    // Modelo de calorias
    var calModel = CaloriesBrain()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.saveButton.layer.cornerRadius = 10
        self.loadButton.layer.cornerRadius = 10
        var cal = self.calModel.getFromLocal()
        loadButton.setTitle("Load: " + String(cal), for: .normal)
        
        self.recipesTable.dataSource = self
        self.recipesTable.delegate = self
        data = self.model.loadByCalories()
        self.recipesTable.reloadData()
        
    }
    
    // Guardar el valor de las calorias
    @IBAction func savePressed(_ sender: UIButton)
    {
        self.calModel.saveLocal(cals: Int(numCalLabel.text!) ?? 500)
        var cal = self.calModel.getFromLocal()
        loadButton.setTitle("Load: " + String(cal), for: .normal)
    }
    
    @IBAction func loadPressed(_ sender: UIButton)
    {
        var cal = self.calModel.getFromLocal()
        print(cal)
        numCalLabel.text = String(cal)
        slider.value = Float(cal)
        loadButton.setTitle("Load: " + String(cal), for: .normal)
        
        //Elegir las recetas
        
        data = model.getRecipesUnderCal(calories: cal)
        self.recipesTable.reloadData()
    }
    
    
    func tableView(_ caloriesTable: UITableView, numberOfRowsInSection section: Int) -> Int
     {
         return data.count
     }
     
     func tableView(_ caloriesTable: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
     {
         let recipe = data[indexPath.row]
         let cell = caloriesTable.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! RecipeCaloriesTableViewCell
         cell.recipeName.text = recipe.name
         cell.recipeType.text = recipe.type
         cell.recipeCalories.text = String(recipe.calories)
         cell.loadImageFromUrl(URLAddress: recipe.img)
         return cell
     }
     
     func tableView(_ caloriesTable: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
         return 180
     }
     
     func collectionSkeletonView(_ skeletonView: UITableView, cellIdentifierForRowAt indexPath: IndexPath) -> ReusableCellIdentifier {
         return RecipeCaloriesTableViewCell.identifier
     }
     
     func loadImageFromUrl(URLAddress: String, productImage:UIImageView) {
         productImage.pin_updateWithProgress = true
         productImage.pin_setImage(from: URL(string: URLAddress)!)
     }

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
