//
//  RecipesViewController.swift
//  Cookey
//
//  Created by Juan Sebastian Ramirez on 6/10/22.
//

import UIKit
import PINRemoteImage

var selectedRecipe: Recipe = Recipe(id: "", calories: 0, country: "", img: "", likeBy: 0, name: "", notes: "", type: "")

@available(iOS 13.0, *)
class RecipesViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    //Singleton del cache
    let manager = CachingManager.instance
    
    //Referencia al model
    var model = RecipeBrain()
    
    var recipes = [Recipe]()

    //Button of the recommendations
    @IBOutlet weak var recomButton: UIButton!
    
    // Conexion con la view
    @IBOutlet weak var recipeCollectionView: UICollectionView!
    
    // Icono de carga
    @IBOutlet weak var carga: UIActivityIndicatorView!
    
    // Label que indica que conectividad hay
    @IBOutlet weak var ConnectivityLaberl: UILabel!
    
    
    //Funcion para cargar las cosas de cache
    func saveToCache(reseta: Recipe)
    {
        manager.add(recipe: reseta, name: reseta.name)
    }
    
    //Funcion para guardar todas las recetas actuales en cache
    func saveAllRecipesInCache()
    {
        for act in recipes
        {
            self.saveToCache(reseta: act)
            print("Agregando " + act.name)
        }
    }
    
    // Funcion para eliminar elementos de cache
    func removeFromCache(reseta: Recipe)
    {
        manager.remove(name: reseta.name)
    }
    
    //Funcion para traer de cachce
    func getAllFromCache() -> [Recipe]?
    {
        return manager.getAll()
    }
    
    // Barra de busqueda in
    override func viewDidLoad()
    {
        //Cuando la vista carga, se asignan los datos y el delegado para que manejen la collection view
        super.viewDidLoad()
        self.recipeCollectionView.dataSource = self
        self.recipeCollectionView.delegate = self
        self.recipeCollectionView.collectionViewLayout =  UICollectionViewFlowLayout()
        recomButton.layer.cornerRadius = 10
        carga.startAnimating()
        
    }
    
    // Una ves que ya sale todo correctamente
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        
        
        
        //Thread para la carga de datos cuando no hay internet
        DispatchQueue.main.async(execute:
        {
            // El label indica que se esta cargando de cache
            self.ConnectivityLaberl.text = ""
            
            //Lista de las recetas en cache
            let cachedRecipes = self.getAllFromCache()
            
            //Semaforo de control
            let semaphore = DispatchSemaphore(value: 1)
            
            if !cachedRecipes!.isEmpty && !Connectivity.isConnectedToInternet
            {
                // Label que indica que cargo de cache porque no hay señal
                self.ConnectivityLaberl.text = "No signal, data loaded from cache"
                
                //Despliegue de las recetas en la vista
                DispatchQueue.global().async
                {
                    semaphore.wait()
                    self.recipes = cachedRecipes!
                    self.model.todas = self.recipes
                    print("Cached recipes loaded")
                    
                    DispatchQueue.main.async(execute: {
                        self.carga.alpha = 0
                        self.recipeCollectionView.reloadData()
                        print("Recipes Displayed")
                    })
                    semaphore.signal()
                }
            }
            
            else if !cachedRecipes!.isEmpty && Connectivity.isConnectedToInternet
            {
                // Label que indica que cargo de cache porque no hay señal
                self.ConnectivityLaberl.text = "Data loaded from cache"
                
                //Despliegue de las recetas en la vista
                DispatchQueue.global().async
                {
                    semaphore.wait()
                    self.recipes = cachedRecipes!
                    self.model.todas = self.recipes
                    print("Cached recipes loaded")
                    
                    DispatchQueue.main.async(execute: {
                        self.carga.alpha = 0
                        self.recipeCollectionView.reloadData()
                        print("Recipes Displayed")
                    })
                    semaphore.signal()
                }
            }
            
            
            // Carga de los productos no hay señal
            else if !Connectivity.isConnectedToInternet
            {
                semaphore.wait()
                print("No internet, loading from local")
                
                self.ConnectivityLaberl.text = "No signal, data loaded from local"
                
                self.model.loadLocal()
                semaphore.signal()
                
                //Despliegue de las recetas en la vista
                DispatchQueue.global().async
                {
                    semaphore.wait()
                    self.recipes = self.model.getRecipes()
                    print("Local recipes sent")
                    
                    self.ConnectivityLaberl.text = "No signal, loaded from local"
                    
                    DispatchQueue.main.async(execute: {
                        self.carga.alpha = 0
                        self.recipeCollectionView.reloadData()
                        print("Recipes Displayed")
                    })
                    semaphore.signal()
                }
            }
            
            // Carga de los productos si hay señal
            else
            {
                self.ConnectivityLaberl.text = "Data loaded from internet"
                
                print("Loading from firebase")
                DispatchQueue.global().async
                {
                    semaphore.wait()
                    self.model.loadAllRecipes(semaphore: semaphore)
                    print("Loaded")
                }
                
                //Despliegue de las recetas en la vista
                DispatchQueue.global().async
                {
                    semaphore.wait()
                    self.recipes = self.model.getRecipes()
                    self.saveAllRecipesInCache()
                    DispatchQueue.main.async(execute: {
                        print("Showing up")
                        self.carga.alpha = 0
                        self.recipeCollectionView.reloadData()
                    })
                    semaphore.signal()
                }
            }
            
            
        })
        
    }
    
    //Cuando presionan el boton de recomendaciones
    @IBAction func recomendationPressed(_ sender: UIButton)
    {
        performSegue(withIdentifier: "Recommendations", sender: self)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "Recommendations"
        {
            let vc = segue.destination as! RecommendationsViewController
            vc.model = self.model
        }
        else
        {
            _ = segue.destination as! DetailedRecipeViewController
        }
    }
    
    @IBAction func recipeTapped(_ sender: UIButton) {
        
        let indexPath2 = IndexPath(row: sender.tag, section: 0)
        let detailed = self.storyboard?.instantiateViewController(identifier: "DetailedRecipe") as! DetailedRecipeViewController
        detailed.recipe = self.recipes[indexPath2.row]
        
        //Reemplazar la receta seleccionada
        selectedRecipe.id = detailed.recipe!.id
        selectedRecipe.calories = detailed.recipe!.calories
        selectedRecipe.country = detailed.recipe!.country
        selectedRecipe.img = detailed.recipe!.img
        selectedRecipe.likeBy = detailed.recipe!.likeBy
        selectedRecipe.name = detailed.recipe!.name
        selectedRecipe.notes = detailed.recipe!.notes
        selectedRecipe.type = detailed.recipe!.type
        
        let recipeName = Notification.Name(recipeNotificationKey)
        NotificationCenter.default.post(name: recipeName, object: selectedRecipe)
        
    }
    
}





// Extension para poder transformar los cells
@available(iOS 13.0, *)
extension RecipesViewController
{
    
    // Numero de recetas
    func collectionView(_ recipeCollectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return self.recipes.count
    }
    
    // Desplegar las celdas
    func collectionView(_ recipeCollectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = recipeCollectionView.dequeueReusableCell(withReuseIdentifier: "RecipeCollectionViewCell", for: indexPath) as! RecipeCollectionViewCell
        cell.setup(with: self.recipes[indexPath.row])
        cell.recipeButton.tag = indexPath.row
        cell.recipeButton.addTarget(self, action: #selector(viewDetailed), for: .touchUpInside)
        
        return cell
    }
    
    @objc func viewDetailed( sender: UIButton){
        
        let indexPath1 = IndexPath(row: sender.tag, section: 0)
        let detailed = self.storyboard?.instantiateViewController(identifier: "DetailedRecipe") as! DetailedRecipeViewController
        detailed.recipe = self.recipes[indexPath1.row]
        self.navigationController?.pushViewController(detailed, animated: true)
        selectedRecipe = detailed.recipe!
        
    }
}

// Extension para arreglar las cells
@available(iOS 13.0, *)
extension RecipesViewController: UICollectionViewDelegateFlowLayout
{
    func collectionView(_ recipeCollectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 160, height: 170)
    }
}


/**
 Clase en singleton para manejar el caching de recipes
 Una vez que carguen la primera vez, ya no deberian requerir volver a cargar debido a que queda en cache
 */
class CachingManager
{
    static let instance = CachingManager()
    
    private init()
    {
    }
    
    var recipesCache: NSCache<NSString, Recipe> =
    {
        let cache = NSCache<NSString, Recipe>()
        cache.countLimit = 100
        cache.totalCostLimit = 1024 * 1024 * 10 // 10mb
        return cache
    }()
    
    // Agrega las recetas al cache
    func add(recipe: Recipe, name: String)
    {
        recipesCache.setObject(recipe, forKey: name as NSString)
        print("Receta " + name + " enviada al cache")
    }
    
    // Remueve las recetas del cache
    func remove (name: String)
    {
        recipesCache.removeObject(forKey: name as NSString)
        print("Receta " + name + " eliminada del cache")
    }
    
    // Para traer el cache
    func get (name: String) -> Recipe?
    {
        return recipesCache.object(forKey: name as NSString)
    }
    
    // Para traer todas las recetas guardadas en cache
    func getAll() -> [Recipe]?
    {
        let all = recipesCache.value(forKey: "allObjects") as? [Recipe]
        return all
    }
}
