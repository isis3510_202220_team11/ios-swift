//
//  InventoryTableViewCell.swift
//  Cookey
//
//  Created by Santiago Triana on 30/10/22.
//

import UIKit

class InventoryTableViewCell: UITableViewCell {
    
    static let identifier = "InventoryTableViewCell"
    var viewControllerParent: InventoryViewController!
    var producto:InventoryProduct = InventoryProduct(id: "", quantity: 1, name: "", unit: "", image: "")
    @IBOutlet var titulo: UILabel!
    @IBOutlet var imagen:UIImageView!
    @IBOutlet weak var deleteButton:UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func deleteButtonPressed(){
        print(producto.id)
        DispatchQueue.main.async(execute: {
            print("todo bien1")
            let nextViewController = self.viewControllerParent.storyboard?.instantiateViewController(withIdentifier: "DeleteProduct") as! DeleteProductViewController
            print("todo bien2")
            print(nextViewController)
            nextViewController.setProducto(producto:self.producto)
            print("todo bien3")
            self.viewControllerParent.present(nextViewController, animated:true, completion:nil)
            print("todo bien4")
            nextViewController.iniciarIconoCarga()
            print("todo bien5")
        })
    }
    
    

}
