//
//  DeleteProductViewController.swift
//  Cookey
//
//  Created by Santiago Triana on 29/11/22.
//

import UIKit
import AudioToolbox

class DeleteProductViewController:UIViewController{
    
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var iconoCarga: UIActivityIndicatorView!
    @IBOutlet weak var productNameLabel: UILabel!
    let imprimirMensajes = true
    let model = DeleteProductBrain()
    
    
    let colorMorado = UIColor(red: 75/255, green: 51/255, blue: 252/255, alpha: 1)
    let colorGris = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.5)
    
    var producto:InventoryProduct = InventoryProduct(id: "", quantity: 1, name: "", unit: "", image: "")
    
    func setProducto(producto:InventoryProduct){
        self.producto = producto
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if(Connectivity.isConnectedToInternet){
            productNameLabel.text = producto.name+" - " + String(producto.quantity) + " " + producto.unit
            statusLabel.text = "Deleting product..."
            statusLabel.tintColor = colorGris
        }else{
            productNameLabel.text = "Not available"
            statusLabel.text = "⚠️ We couldn't reach internet connection, so product can't be deleted"
            statusLabel.tintColor = colorMorado
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if(Connectivity.isConnectedToInternet){
            let semaphore = DispatchSemaphore(value: 1)
            
            DispatchQueue.global().async {
                semaphore.wait()
                self.model.deleteProduct(semaphore: semaphore, product: self.producto)
            }
            
            DispatchQueue.global().async {
                semaphore.wait()
                if Connectivity.isConnectedToInternet && self.model.success{
                    DispatchQueue.main.async(execute: {
                        self.statusLabel.tintColor = self.colorMorado
                        self.statusLabel.text="✅ Product deleted correctly"
                        AudioServicesPlayAlertSoundWithCompletion(SystemSoundID(kSystemSoundID_Vibrate)) { }
                        self.detenerIconoCarga()
                    })
                    DispatchQueue.main.asyncAfter(deadline: .now()+3, execute: {
                        //Esta linea mata todas las vistas
                        var rootViewController = self.view.window?.rootViewController
                        rootViewController?.dismiss(animated: true, completion: nil)
                        //esta vuelve y lanza el inventario
                        let storyboard = UIStoryboard(name: "Inventory" , bundle: nil)
                        let inventoryViewController = storyboard.instantiateInitialViewController()
                        rootViewController?.present(inventoryViewController!, animated:true, completion:nil)
                        
                    })
                }else{
                    DispatchQueue.main.async(execute: {
                        self.statusLabel.text="⚠️ Product couldn't be deleted"
                        self.statusLabel.tintColor = self.colorMorado
                        self.detenerIconoCarga()
                    })
                }
                semaphore.signal()
            }
        }else{
            detenerIconoCarga()
        }
    }
    
    func iniciarIconoCarga(){
        if iconoCarga != nil {
            iconoCarga.isHidden=false
            iconoCarga.startAnimating()
        }
    }
    
    func detenerIconoCarga(){
        iconoCarga.stopAnimating()
        iconoCarga.isHidden=true
    }
    
    func imprimir(_ mensaje:String){
        if(imprimirMensajes){
            print("DeleteProductViewController:"+mensaje)
        }
    }
    
}
