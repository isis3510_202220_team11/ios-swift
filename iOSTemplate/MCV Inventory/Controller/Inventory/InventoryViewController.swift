//
//  InventoryViewController.swift
//  Cookey
//
//  Created by Juan Jose on 23/09/22.
//

import UIKit
import SkeletonView
import PINRemoteImage

class InventoryViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, SkeletonTableViewDataSource{
    
    // Tabla que va a mostrar los ingredientes en el inventario del usuario
    
    let imprimirMensajes = true
    let colorMorado = UIColor(red: 75/255, green: 51/255, blue: 252/255, alpha: 1)
    let colorGris = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.5)
    
    let model = InventoryBrain()
    var data = [String]()
    var imageData = [String]()
    var idData = [InventoryProduct]()
    
    func imprimir (_ mensaje:String){
        if imprimirMensajes{
            print("InventoryViewContoller: "+mensaje)
        }
    }
    
    
    @IBOutlet var connectivityInformationLabel: UILabel!
    @IBOutlet var inventoryTable: UITableView!
    @IBOutlet weak var addNewProductButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        connectivityInformationLabel.text = ""
        inventoryTable.rowHeight = 120
        inventoryTable.estimatedRowHeight=120
        inventoryTable.dataSource = self
        inventoryTable.delegate = self
        addNewProductButton.layer.cornerRadius=10
        inventoryTable.isSkeletonable = true
        if #available(iOS 13.0, *) {
            inventoryTable.showAnimatedGradientSkeleton(usingGradient: .init(baseColor: colorMorado), animation: nil, transition: .crossDissolve(0.25))
        } else {
            // Fallback on earlier versions
        }
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        //inventoryTable.showAnimatedSkeleton(usingColor: .concrete, transition: .crossDissolve(0.25))
        
        DispatchQueue.main.async(execute: {
            
            let semaphore = DispatchSemaphore(value: 1)
            self.connectivityInformationLabel.text="Syncing..."
            var products:[InventoryProduct] = self.model.getProductsLocally()
            self.imprimir("Cargando los productos que se encuentran en local")
            if products.count != 0 {
                self.loadProductsUI(productos: products)
            }
            
            DispatchQueue.global().async {
                semaphore.wait()
                self.imprimir("Inicio proceso de busqueda productos en DB")
                self.model.loadAllProducts(semaphore: semaphore)
            }
            
            DispatchQueue.global().async {
                semaphore.wait()
                self.imprimir("Solicitando productos finalmente y actualizando en UI")
                var products = self.model.getProducts()
                self.loadProductsUI(productos: products)
                semaphore.signal()
                if Connectivity.isConnectedToInternet{
                    DispatchQueue.main.async(execute: {self.connectivityInformationLabel.text="Synced ✅"})
                    DispatchQueue.main.asyncAfter(deadline: .now()+3, execute: {self.connectivityInformationLabel.text=""})
                }else{
                    DispatchQueue.main.async(execute: {self.connectivityInformationLabel.text="⚠️ Connection error, showing local inventory"})
                }
            }
        })
    }
    
    @IBAction func closeView(){
        self.dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func collectionSkeletonView(_ skeletonView: UITableView, cellIdentifierForRowAt indexPath: IndexPath) -> ReusableCellIdentifier {
        return InventoryTableViewCell.identifier
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: InventoryTableViewCell.identifier, for: indexPath) as! InventoryTableViewCell
        if !data.isEmpty{
            cell.viewControllerParent=self
            cell.producto = self.idData[indexPath.row]
            cell.titulo.text = self.data[indexPath.row]
            if #available(iOS 13.0, *) {
                cell.imagen.image = UIImage(systemName: "photo")
                loadImageFromUrl(URLAddress: imageData[indexPath.row], productImage: cell.imagen)
                
            } else {
                // Fallback on earlier versions
            }
        }
        return cell
    }
    
    func loadProductsUI(productos:[InventoryProduct]){
        data = []
        imageData = []
        idData = []
        for producto in productos {
            self.data.append(producto.name+" - " + String(producto.quantity) + " " + producto.unit)
            self.imageData.append(producto.image)
            self.idData.append(producto)
        }
        
        DispatchQueue.main.async(execute: {
            self.inventoryTable.reloadData()
            self.inventoryTable.stopSkeletonAnimation()
            self.view.hideSkeleton(reloadDataAfter: true, transition: .crossDissolve(0.25))
            
        })
    }
    
    func loadImageFromUrl(URLAddress: String, productImage:UIImageView) {
        if(URLAddress != ""){
            productImage.pin_updateWithProgress = true
            productImage.pin_setImage(from: URL(string: URLAddress) ?? URL(string: "https://cdn-icons-png.flaticon.com/512/985/985515.png"))
        }else{
            productImage.pin_setImage(from: URL(string: "https://cdn-icons-png.flaticon.com/512/985/985515.png"))
        }
        
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
