//
//  PromosViewController.swift
//  Cookey
//
//  Created by Juan Jose on 5/11/22.
//

import UIKit
import PINRemoteImage

var selectedPromo = Promo(id: "", name: "", after: "", before: "", discount: 0, place: "", description: "", img: "")

@available(iOS 13.0, *)
class PromosViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {

    //Referencia al model
    var model = PromoBrain()
    
    //Referencia a las promociones existentes
    var promos = [Promo]()
    
    //Conexion con la view
    @IBOutlet weak var promoCollectionView: UICollectionView!
    
    //Icono de carga, mientras se muestran las promociones
    @IBOutlet weak var loadIcon: UIActivityIndicatorView!
    
    //Mostrar el status de carga de la vista
    @IBOutlet weak var statusLabel: UILabel!
    
    //Boton para los consejos de promociones
    @IBOutlet weak var adviceButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        adviceButton.layer.cornerRadius = 10
        // Al cargar la vista completa se tienen que asignar los datos y el delegate para poder controlar la coleccion.
        self.promoCollectionView.dataSource = self
        self.promoCollectionView.delegate = self
        self.promoCollectionView.collectionViewLayout =  UICollectionViewFlowLayout()
        loadIcon.startAnimating()
        
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        self.statusLabel.text = "Syncing..."
        //Thread para la carga de datos cuando no hay internet
        DispatchQueue.main.async(execute:
        {
            let semaphore = DispatchSemaphore(value: 1)
            
            // Carga de las promociones cuando no hay señal
            if !Connectivity.isConnectedToInternet
            {
                self.statusLabel.text = "⚠️ Connectivity trouble, showing local storage"
                semaphore.wait()
                print("No internet, loading from local")
                self.model.loadLocal()
                semaphore.signal()
                
                //Despliegue de las promociones en la vista
                DispatchQueue.global().async
                {
                    semaphore.wait()
                    self.promos = self.model.getPromos()
                    print("Local promos sent")
                    
                    DispatchQueue.main.async(execute: {
                        self.loadIcon.alpha = 0
                        self.promoCollectionView.reloadData()
                        print("Promos Displayed")
                    })
                    semaphore.signal()
                }
            }
            
            // Carga de los promociones si hay señal
            else
            {
                self.statusLabel.text = "Synced ✅"
                print("Loading from firebase")
                DispatchQueue.global().async
                {
                    semaphore.wait()
                    self.model.loadAllPromos(semaphore: semaphore)
                    print("Loaded")
                }
                
                //Despliegue de las recetas en la vista
                DispatchQueue.global().async
                {
                    semaphore.wait()
                    self.promos = self.model.getPromos()
                    DispatchQueue.main.async(execute: {
                        print("Showing up")
                        self.loadIcon.alpha = 0
                        self.promoCollectionView.reloadData()
                    })
                    semaphore.signal()
                }
                DispatchQueue.main.asyncAfter(deadline: .now()+3, execute: {self.statusLabel.text=""})
            }
            
            
        })
        
    }
    
    @IBAction func promoTapped(_ sender: UIButton) {
        
        let indexPath3 = IndexPath(row: sender.tag, section: 0)
        let detailed = self.storyboard?.instantiateViewController(identifier: "DetailedPromo") as! DetailedPromoViewController
        detailed.promo = self.promos[indexPath3.row]
        
        //Reemplazar la promo seleccionada
        selectedPromo.id = detailed.promo!.id
        selectedPromo.name = detailed.promo!.name
        selectedPromo.before = detailed.promo!.before
        selectedPromo.after = detailed.promo!.after
        selectedPromo.place = detailed.promo!.place
        selectedPromo.discount = detailed.promo!.discount
        selectedPromo.description = detailed.promo!.description
        selectedPromo.img = detailed.promo!.img
        
        print("La des es: " + selectedPromo.description)
        
        let promoName = Notification.Name(promoNotificationKey)
        NotificationCenter.default.post(name: promoName, object: selectedPromo)
    }
    
}

// Extension para poder transformar los cells
@available(iOS 13.0, *)
extension PromosViewController
{
    
    // Numero de promos
    func collectionView(_ promoCollectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return self.model.lista.count
    }
    
    // Desplegar las celdas
    func collectionView(_ promoCollectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = promoCollectionView.dequeueReusableCell(withReuseIdentifier: "PromoCollectionViewCell", for: indexPath) as! PromoCollectionViewCell
        cell.setup(with: self.model.lista[indexPath.row])
        cell.promoButton.tag = indexPath.row
        cell.promoButton.addTarget(self, action: #selector(viewDetailed2), for: .touchUpInside)
        
        return cell
    }
    
    @objc func viewDetailed2( sender: UIButton){
        
        let indexPath4 = IndexPath(row: sender.tag, section: 0)
        let detailed = self.storyboard?.instantiateViewController(identifier: "DetailedPromo") as! DetailedPromoViewController
        detailed.promo = self.promos[indexPath4.row]
        self.navigationController?.pushViewController(detailed, animated: true)
        selectedPromo = detailed.promo!
    }
}

// Extension para arreglar las cells
@available(iOS 13.0, *)
extension PromosViewController: UICollectionViewDelegateFlowLayout
{
    func collectionView(_ promoCollectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(
            width: 349,
            height: 170
        )
    }
}
