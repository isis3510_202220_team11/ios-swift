//
//  AdviceViewController.swift
//  Cookey
//
//  Created by Juan Jose on 30/11/22.
//

import UIKit
import DropDown

class AdviceViewController: UIViewController {

    @IBOutlet weak var dropDownView: UIView!
    
    @IBOutlet weak var marketLabel: UILabel!
    
    @IBOutlet weak var checkButton: UIButton!
    
    @IBOutlet weak var promoImg: UIImageView!
    
    @IBOutlet weak var beforeLabel: UILabel!
    
    @IBOutlet weak var afterLabel: UILabel!
    
    @IBOutlet weak var placeLabel: UILabel!
    
    @IBOutlet weak var discountLabel: UILabel!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var descriptionLabel: UILabel!
    
    let model = PromoBrain()
    
    let dropDown = DropDown()
    
    let marketArray = ["No selected market", "Exito", "Carulla", "D1", "Alkosto", "Farmatodo"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let semaphore = DispatchSemaphore(value: 1)
        
        if Connectivity.isConnectedToInternet {
            model.loadAllPromos(semaphore: semaphore)
        }else{
            model.loadLocal()
        }
        
        let count = 0...9
        
        dropDown.anchorView = dropDownView
        dropDown.dataSource = marketArray
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.marketLabel.text = marketArray[index]
            
            var mayorDiscount = 0
            var promoMarket = model.lista[1]
            
            for number in count {
                if(marketLabel.text!.trimmingCharacters(in: .whitespacesAndNewlines) == model.lista[number].place.trimmingCharacters(in: .whitespacesAndNewlines)) {
                    
                    if model.lista[number].discount >= mayorDiscount {
                        mayorDiscount = model.lista[number].discount
                        promoMarket = model.lista[number]
                    }
                    
                }
                
                if(marketLabel.text == "No selected market"){
                    if model.lista[number].discount >= mayorDiscount {
                        mayorDiscount = model.lista[number].discount
                        promoMarket = model.lista[number]
                    }
                }
                
            }
            
            self.loadImageFromUrl(URLAddress: promoMarket.img, promoImage: self.promoImg)
            beforeLabel.text = promoMarket.before
            afterLabel.text = promoMarket.after
            placeLabel.text = promoMarket.place
            discountLabel.text = String(promoMarket.discount) + "%"
            nameLabel.text = promoMarket.name
            descriptionLabel.text = promoMarket.description
            
        }
    }
    
    func loadImageFromUrl(URLAddress: String, promoImage:UIImageView) {
        promoImage.pin_updateWithProgress = true
        promoImage.pin_setImage(from: URL(string: URLAddress)!)
    }
    
    @IBAction func checkTapped(_ sender: Any) {
        dropDown.show()
        
    }
    
}
