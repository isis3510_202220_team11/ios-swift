//
//  DetailedPromoViewController.swift
//  Cookey
//
//  Created by Juan Jose on 30/11/22.
//

import UIKit

let promoNotificationKey = "co.cooKey.promo"

class DetailedPromoViewController: UIViewController {
    
    //Promocion que se va a mostrar
    weak var promo : Promo?

    //Nombre de la promocion
    @IBOutlet weak var promoTitle: UILabel!
    
    //Imagen de la promocion
    @IBOutlet weak var promoImg: UIImageView!
    
    //Precio anterior
    @IBOutlet weak var beforeLabel: UILabel!
    
    //Precio despues
    @IBOutlet weak var afterLabel: UILabel!
    
    //Lugar de venta
    @IBOutlet weak var placeLabel: UILabel!
    
    //Porcentaje de descuento
    @IBOutlet weak var discountLabel: UILabel!
    
    //Descripcion de la oferta
    @IBOutlet weak var descriptionLabel: UILabel!
    
    //Key de la notificacion de la receta
    let promoKey = Notification.Name(rawValue: promoNotificationKey)
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        promoTitle.text = promo?.name
        beforeLabel.text = promo?.before
        afterLabel.text = promo?.after
        placeLabel.text = promo?.place
        discountLabel.text = String(promo?.discount ?? 0)
        descriptionLabel.text = promo?.description

        print("el detailed es: "+selectedPromo.name)
        
        createObservers()
    }
    
    func createObservers() {
        // Promocion
        NotificationCenter.default.addObserver(self, selector: #selector(DetailedPromoViewController.updatePromoName(notification:)), name: promoKey, object: selectedPromo)
        
        NotificationCenter.default.addObserver(self, selector: #selector(DetailedPromoViewController.updatePromoImage(notification:)), name: promoKey, object: selectedPromo)
        
        NotificationCenter.default.addObserver(self, selector: #selector(DetailedPromoViewController.updatePromoBefore(notification:)), name: promoKey, object: selectedPromo)
        
        NotificationCenter.default.addObserver(self, selector: #selector(DetailedPromoViewController.updatePromoAfter(notification:)), name: promoKey, object: selectedPromo)
        
        NotificationCenter.default.addObserver(self, selector: #selector(DetailedPromoViewController.updatePromoPlace(notification:)), name: promoKey, object: selectedPromo)
        
        NotificationCenter.default.addObserver(self, selector: #selector(DetailedPromoViewController.updatePromoDiscount(notification:)), name: promoKey, object: selectedPromo)
        
        NotificationCenter.default.addObserver(self, selector: #selector(DetailedPromoViewController.updatePromoDescription(notification:)), name: promoKey, object: selectedPromo)
    }
    
    // Funciones para cambiar los elementos por medio del observer
    @objc func updatePromoName(notification: NSNotification) {
        
        promoTitle.text = selectedPromo.name
        
    }
    
    @objc func updatePromoImage(notification: NSNotification) {
        
        self.loadImageFromUrl(URLAddress: selectedPromo.img, promoImage: self.promoImg)
        
    }
    
    @objc func updatePromoBefore(notification: NSNotification) {
        
        beforeLabel.text = selectedPromo.before
        
    }
    
    @objc func updatePromoAfter(notification: NSNotification) {
        
        afterLabel.text = selectedPromo.after
        
    }
    
    @objc func updatePromoPlace(notification: NSNotification) {
        
        placeLabel.text = selectedPromo.place
        
    }
    
    @objc func updatePromoDiscount(notification: NSNotification) {
        
        discountLabel.text = String(selectedPromo.discount) + "%"
        
    }
    
    @objc func updatePromoDescription(notification: NSNotification) {
        
        descriptionLabel.text = selectedPromo.description
        
    }
    
    func loadImageFromUrl(URLAddress: String, promoImage:UIImageView) {
        promoImage.pin_updateWithProgress = true
        promoImage.pin_setImage(from: URL(string: URLAddress)!)
    }

}
