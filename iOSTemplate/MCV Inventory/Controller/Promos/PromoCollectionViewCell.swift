//
//  PromoCollectionViewCell.swift
//  Cookey
//
//  Created by Juan Jose on 5/11/22.
//

import UIKit

class PromoCollectionViewCell: UICollectionViewCell {
    //Guarda el nombre del producto en promocion
    @IBOutlet weak var nameLabel: UILabel!
    
    //Guarda el precio anterior
    @IBOutlet weak var beforeLabel: UILabel!
    
    //Guarda el precio actual
    @IBOutlet weak var afterLabel: UILabel!
    
    //Guarda el descuento del producto
    @IBOutlet weak var discountLabel: UILabel!
    
    //Guarda la imagen de la promo
    @IBOutlet weak var imgView: UIImageView!
    
    //Promocion actual
    weak var promoActual: Promo?
    
    //Seleccion de la promo
    @IBOutlet weak var promoButton: UIButton!
    
    // Va a llenar la cell con los elementos
    func setup(with promo: Promo)
    {
        self.loadImageFromUrl(URLAddress: promo.img, promoImage: self.imgView)
        nameLabel.text = promo.name
        beforeLabel.text = promo.before
        afterLabel.text = promo.after
        discountLabel.text = String(promo.discount) + "% off"
        promoActual = promo
    }
    
    func loadImageFromUrl(URLAddress: String, promoImage:UIImageView) {
        promoImage.pin_updateWithProgress = true
        promoImage.pin_setImage(from: URL(string: URLAddress)!)
    }
    
}
