



//
//  AddNewProductViewController.swift
//  Cookey
//
//  Created by Santiago Triana on 27/09/22.
//
import UIKit
import os.log

class StartScanningViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var startScanningButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if(Connectivity.isConnectedToInternet){
            startScanningButton.layer.cornerRadius = 10
            descriptionLabel.text = "Add your products to your CooKey App scanning the barcode on the side of it."
        }else{
            startScanningButton.isHidden = true
            descriptionLabel.text = "⚠️ Connection error, camara product adding is disabled"
        }
    }
    
    
    @IBAction func closeView(){
        self.dismiss(animated: true, completion: nil)
    }
    
}
