//
//  ConfirmAddNewProductViewController.swift
//  Cookey
//
//  Created by Santiago Triana on 8/11/22.
//

import Foundation
import UIKit

import AudioToolbox

class ConfirmAddNewProductViewController: UIViewController, UITextFieldDelegate {
    
    
    let imprimirMensajes = true
    
    let colorMorado = UIColor(red: 75/255, green: 51/255, blue: 252/255, alpha: 1)
    let colorGris = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.5)
    let unit_options = ["g", "Kg", "ml", "L", "Units"]
    
    @IBOutlet weak var substractButton: UIButton!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var plusButton: UIButton!
    
    @IBOutlet weak var informationLabel: UILabel!
    @IBOutlet weak var quantityTextField: UITextField!
    
    @IBOutlet weak var productNameTextField: UITextField!
    
    @IBOutlet weak var iconoCarga: UIActivityIndicatorView!
    @IBOutlet weak var productUnitTextField: UITextField!
    
    var productImage:String = ""
    var productId:String = "-1"
    
    var productoInicial:InventoryProduct = InventoryProduct(id: "", quantity: 1, name: "", unit: "g", image: "")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addButton.layer.cornerRadius=10
        plusButton.tintColor = colorMorado
        substractButton.tintColor = colorGris
        quantityTextField.text = "1"
        quantityTextField.delegate = self
        quantityTextField.tag=1
        productNameTextField.delegate = self
        productNameTextField.tag=2
        productUnitTextField.delegate = self
        productUnitTextField.tag=3
        informationLabel.text=""
        informationLabel.textColor = colorMorado
        checkDisplayOfAddButton()
        iconoCarga.isHidden=true
        productUnitTextField.loadDropdownData(data:unit_options)
        //Las siguientes dos lineas son para ocultar el teclado cuando se toca otro lugar que no son botones ni textfields
        let tap = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
    }
    
    /**
     Esta funcion sirve para guardar el teclado, es llamada en el viewDidLoad
     */
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    override func viewDidAppear(_ a:Bool){
        super.viewDidAppear(a)
        setValues(product:productoInicial)
    }
    
    @IBAction func substractButtonPressed(){
        quantityTextFieldEdited()
        //deselectQuantityTextField()
        let quantityString = quantityTextField.text
        var quantityInteger:Int = Int(quantityString ?? "0")!
        if(quantityInteger>1){
            quantityInteger -= 1
        }
        updateAddSubstractButtonsColors(number:quantityInteger)
        quantityTextField.text = String(quantityInteger)
        imprimir("Substract button presionado")
    }
    
    @IBAction func plusButtonPressed(){
        quantityTextFieldEdited()
        //deselectQuantityTextField()
        let quantityString = quantityTextField.text
        var quantityInteger:Int? = Int(quantityString ?? "0")
        quantityInteger! += 1
        updateAddSubstractButtonsColors(number:quantityInteger!)
        quantityTextField.text = String(quantityInteger!)
        imprimir("Plus button presionado")
    }
    
    @IBAction func quantityTextFieldEdited(){
        var quantityString:String = quantityTextField.text ?? "0"
        imprimir("cantidad cambiada: " + quantityString)
        if(quantityString==""){quantityString="0"}
        var quantityInteger:Int = Int(quantityString)!
        if(quantityInteger<=0) { quantityInteger=1 }
        
        updateAddSubstractButtonsColors(number:quantityInteger)
        quantityTextField.text = String(quantityInteger)
        
    }
    
    @IBAction func productNameTextFieldEdited(){
        checkDisplayOfAddButton()
    }
    
    @IBAction func productUnitTextFieldEdited(){
        checkDisplayOfAddButton()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField.tag == quantityTextField.tag {
            let currenttxt = textField.text ?? ""
            guard let stringRange = Range(range, in: currenttxt) else {
                return false
            }
            
            let updatetxt = currenttxt.replacingCharacters(in: stringRange, with: string)
            
            return updatetxt.count <= 5
        }else if textField.tag == productUnitTextField.tag {
            let currenttxt = textField.text ?? ""
            guard let stringRange = Range(range, in: currenttxt) else {
                return false
            }
            
            let updatetxt = currenttxt.replacingCharacters(in: stringRange, with: string)
            
            return updatetxt.count <= 8
        }else{
            let currenttxt = textField.text ?? ""
            guard let stringRange = Range(range, in: currenttxt) else {
                return false
            }
            
            let updatetxt = currenttxt.replacingCharacters(in: stringRange, with: string)
            
            return updatetxt.count <= 50
        }
        
    }
    
    func updateAddSubstractButtonsColors(number:Int){
        if(number>1){
            substractButton.isUserInteractionEnabled=true
            substractButton.tintColor = colorMorado
        }
        else{
            substractButton.isUserInteractionEnabled=false
            substractButton.tintColor = colorGris
            
        }
    }
    
    func checkDisplayOfAddButton(){
        if(Connectivity.isConnectedToInternet){
            if(!(quantityTextField.text?.isEmpty ?? true) && !(productNameTextField.text?.isEmpty ?? true) && !(productUnitTextField.text?.isEmpty ?? true)){
                addButton.isHidden=false
            }else{
                addButton.isHidden=true
            }
            informationLabel.text = ""
        }else{
            addButton.isHidden=true
            informationLabel.text = "⚠️ Connection error, products can't be created"
        }
        
    }
    
    func setProductoInicial(productoInicial:InventoryProduct){
        self.productoInicial = productoInicial
    }
    
    func setValues(product:InventoryProduct){
        self.productImage = product.image
        productUnitTextField.text = product.unit
        productNameTextField.text = product.name
        quantityTextField.text = String(product.quantity)
        checkDisplayOfAddButton()
        detenerIconoCarga()
    }
    
    @IBAction func addButtonPressed(){
        addButton.isHidden=true
        iniciarIconoCarga()
        informationLabel.text="Adding product..."
        AddInventoryProductBrain(vistaPadre: self, informationLabel: informationLabel, iconoCarga:iconoCarga).addProduct(producto: InventoryProduct(id: self.productId, quantity: Int(quantityTextField.text!) ?? 0, name: productNameTextField.text!, unit: productUnitTextField.text!, image: self.productImage))
    }
    
    func iniciarIconoCarga(){
        if iconoCarga != nil {
            iconoCarga.isHidden=false
            iconoCarga.startAnimating()
        }
    }
    
    func detenerIconoCarga(){
        iconoCarga.stopAnimating()
        iconoCarga.isHidden=true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switchFields(textField: textField)
        return false
    }
    
    
    
    private func switchFields(textField: UITextField) {
        switch textField {
        case quantityTextField:
            productUnitTextField.becomeFirstResponder()
        case productUnitTextField:
            productNameTextField.becomeFirstResponder()
        case productNameTextField:
            self.view.endEditing(true)
        default:
            break;
        }
    }
    
    func closeView(){
        self.dismiss(animated: true, completion: nil)
    }
    
    func imprimir(_ string:String!){
        if(imprimirMensajes){
            print("ConfirmAddNewProductViewController: " + string)
        }
    }
}
