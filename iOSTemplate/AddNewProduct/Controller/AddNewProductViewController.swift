//
//  AddNewProductViewController.swift
//  Cookey
//
//  Created by Santiago Triana on 27/09/22.
//
import UIKit
import os.log
import Alamofire
import MobileCoreServices

class AddNewProductViewController: UIViewController, UITextFieldDelegate {
    
    let imprimirMensajes = true
    let sURLBarcodeReader:String!="https://barcode-reader-cookey.herokuapp.com/readBarcode"
    let sURLInfoProductsAPI:String!="https://barcode-reader-cookey.herokuapp.com/getBarcodeInfo?barcode="
    
    
    let colorMorado = UIColor(red: 75/255, green: 51/255, blue: 252/255, alpha: 1)
    let colorGris = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.5)
    
    var abrirCamara = true
    
    var imageLink:String = ""
    
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var plusButton: UIView!
    @IBOutlet weak var substractButton: UIView!
    @IBOutlet weak var closeKeyboardButton: UIButton!
    @IBOutlet weak var retakePhotoButton: UIBarButtonItem!
    
    @IBOutlet weak var quantityTextField: UITextField!
    
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var productDescription: UITextView!
    
    @IBOutlet weak var productImage: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addButton.layer.cornerRadius=10
        addButton.isHidden=true
        plusButton.tintColor = colorMorado
        substractButton.tintColor = colorGris
        quantityTextField.text = "1"
        productDescription.text = "Loading..."
        productName.text = " "
        if #available(iOS 13.0, *) {
            productImage.backgroundColor = .secondarySystemBackground
        } else {
            // Fallback on earlier versions
        }
        imprimir("image " + (productImage.image?.description ?? "nil"))
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if(productImage.image?.description == nil && abrirCamara){
            startScanning()
        }
    }
    
    @IBAction func addButtonPressed(){
        var producto = InventoryProduct(id: "", quantity: Int(quantityTextField.text ?? "1")!, name: productDescription.text, unit: "", image: imageLink)
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "ConfirmAddNewProduct") as! ConfirmAddNewProductViewController
        nextViewController.setProductoInicial(productoInicial:producto)
        self.present(nextViewController, animated:true, completion:nil)
        nextViewController.iniciarIconoCarga()
        
    }
    
    @IBAction func retakePhoto(){
        productDescription.text = "Loading..."
        addButton.isHidden=true
        productName.text = " "
        startScanning()
    }
    
    /**
     Funcion que es usada para cerrar el teclado
     */
    @IBAction func closeKeyboard(){
        imprimir("Close keyboard")
        self.view.endEditing(false)
    }
    
    /**
     Funcion que es llamada cuando el boton startScanning es presionado
     */
    @IBAction func startScanning(){
        abrirCamara = false
        let picker = UIImagePickerController()
        //picker.sourceType = .photoLibrary
        picker.sourceType = .camera
        imprimir("Iniciando la camara")
        picker.delegate = self
        present(picker, animated: true)
    }
    
    
    @IBAction func substractButtonPressed(){
        quantityTextFieldEdited()
        //deselectQuantityTextField()
        let quantityString = quantityTextField.text
        var quantityInteger:Int = Int(quantityString ?? "0")!
        if(quantityInteger>1){
            quantityInteger -= 1
        }
        updateAddSubstractButtonsColors(number:quantityInteger)
        quantityTextField.text = String(quantityInteger)
        imprimir("Substract button presionado")
    }
    
    @IBAction func plusButtonPressed(){
        quantityTextFieldEdited()
        //deselectQuantityTextField()
        let quantityString = quantityTextField.text
        var quantityInteger:Int? = Int(quantityString ?? "0")
        quantityInteger! += 1
        updateAddSubstractButtonsColors(number:quantityInteger!)
        quantityTextField.text = String(quantityInteger!)
        imprimir("Add button presionado")
    }
    
    @IBAction func quantityTextFieldEdited(){
        var quantityString:String = quantityTextField.text ?? "0"
        imprimir("cantidad cambiada: " + quantityString)
        if(quantityString==""){quantityString="0"}
        var quantityInteger:Int = Int(quantityString)!
        if(quantityInteger<=0 || quantityInteger>=50000) { quantityInteger=1 }
        
        updateAddSubstractButtonsColors(number:quantityInteger)
        quantityTextField.text = String(quantityInteger)
        
    }
    
    func updateAddSubstractButtonsColors(number:Int){
        if(number>1){
            substractButton.isUserInteractionEnabled=true
            substractButton.tintColor = colorMorado
        }
        else{
            substractButton.isUserInteractionEnabled=false
            substractButton.tintColor = colorGris
            
        }
    }
    
    @IBAction func closeView(){
        self.dismiss(animated: true, completion: nil)
    }
    
    func imprimir(_ string:String!){
        if(imprimirMensajes){
            print("AddNewProductViewController: " + string)
        }
    }
    
    
    
    func getBarcodeInfo(_ codigo:String){
        let serializer = DataResponseSerializer(emptyResponseCodes: Set([200,204,205]))
        
        var request = URLRequest(url: URL(string:sURLInfoProductsAPI+codigo)!)
        
        AF.request(request)
            .uploadProgress{
                progress in
                
            }
            .response(responseSerializer: serializer){ response in
                if(response.error == nil){
                    var responseString:String!
                    responseString=""
                    if(response.data != nil){
                        responseString =  String(bytes:response.data!, encoding: .utf8)
                    }else{
                        responseString=response.response?.description
                    }
                    self.imprimir(responseString)
                    let jsonResponse = self.stringToJson(responseString)
                    let titleReponse:String = jsonResponse["title"] as! String
                    self.updateProductDescription(self.productDescription.text + " " + titleReponse)
                    self.loadImageFromUrl(URLAddress: jsonResponse["imageLink"] as! String)
                    self.imprimir(jsonResponse["title"] as? String)
                }
                
                
            }
    }
    
    /**
     convierte un string en un json
     */
    func stringToJson(_ string:String)->Dictionary<String, Any>{
        let emptyDict: Dictionary<String, Any> = [:]
        let data = string.data(using: .utf8)!
        do {
            if let jsonArray = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? Dictionary<String,Any>
            {
                return jsonArray // use the json here
            } else {
                print("BAD JSON")
                return emptyDict
            }
        } catch let error as NSError {
            print(error)
            return emptyDict
        }
    }
    
    func updateProductDescription(_ newDescription:String){
        productDescription.text = newDescription
    }
    
    func updateProductName(_ newDescription:String){
        productName.text = newDescription
    }
    
    func loadImageFromUrl(URLAddress: String) {
        imageLink = URLAddress
        if(URLAddress != ""){
            productImage.pin_updateWithProgress = true
            productImage.pin_setImage(from: URL(string: URLAddress) ?? URL(string: "https://cdn-icons-png.flaticon.com/512/985/985515.png"))
        }else{
            productImage.pin_setImage(from: URL(string: "https://cdn-icons-png.flaticon.com/512/985/985515.png"))
        }
        
    }
    
}

extension AddNewProductViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
        closeView()
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        guard let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else {
            return
        }
        productImage.image=image
        DispatchQueue.main.async {
            var BI:BarcodeInterpreterService = BarcodeInterpreterAdapter()
            var producto = BI.postRequest(self, oImage: image,  updateProductName:self.updateProductName, updateProductDescription:self.updateProductDescription, addButton:self.addButton, getBarcodeInfo:self.getBarcodeInfo)
        }
    }
    
    
    
    
}
