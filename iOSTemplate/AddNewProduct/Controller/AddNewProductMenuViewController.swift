//
//  AddNewProductMenu.swift
//  Cookey
//
//  Created by Santiago Triana on 8/11/22.
//

import Foundation
import UIKit

class AddNewProductMenuViewController: UIViewController {
    
    let imprimirMensajes = true
    
    @IBOutlet weak var useCameraButton: UIButton!
    
    @IBOutlet weak var manuallyButton: UIButton!
    
    @IBOutlet weak var hotProductButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        useCameraButton.layer.cornerRadius=10
        manuallyButton.layer.cornerRadius=10
        hotProductButton.layer.cornerRadius=10
    }
    
    @IBAction func hotProductButtonPressed(){
        
        let semaphore = DispatchSemaphore(value: 1)
        
        let model = HotProductBrain()
        
        
        
        DispatchQueue.global().async {
            semaphore.wait()
            self.imprimir("Inicio proceso de busqueda hot product en DB")
            model.loadHotProduct(semaphore: semaphore)
        }
        
        DispatchQueue.global().async {
            semaphore.wait()
            let producto = model.getHotProduct()
            semaphore.signal()
            DispatchQueue.main.async(execute: {
                let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "ConfirmAddNewProduct") as! ConfirmAddNewProductViewController
                nextViewController.setProductoInicial(productoInicial:producto)
                self.present(nextViewController, animated:true, completion:nil)
                nextViewController.iniciarIconoCarga()
            })
            
        }
        
    }
    
    func imprimir(_ msje:String){
        if(imprimirMensajes){
            print("AddNewProductMenuViewController: " + msje)
        }
    }
    
    
}
