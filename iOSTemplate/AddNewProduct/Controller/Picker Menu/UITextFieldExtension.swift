//
//  UITextFieldExtension.swift
//  Cookey
//
//  Created by Santiago Triana on 1/12/22.
//

import Foundation
import UIKit
 
extension UITextField {
    func loadDropdownData(data: [String]) {
        self.inputView = UnitPickerView(pickerData: data, dropdownField: self)
    }}
