//
//  AddInventoryProductBrain.swift
//  Cookey
//
//  Created by Santiago Triana on 9/11/22.
//

import Foundation
import Firebase
import AudioToolbox

class AddInventoryProductBrain {
    let imprimirMensajes = true
    
    //Create cleaned versions of the data
    var productName:String  = ""
    var productImage:String = ""
    var productQuantity:Int = 0
    var productUnit:String = ""
    var vistaPadre:UIViewController? = nil
    var informationLabel:UILabel? = nil
    var iconoCarga:UIActivityIndicatorView? = nil
    
    init(vistaPadre:UIViewController, informationLabel:UILabel, iconoCarga:UIActivityIndicatorView) {
        self.vistaPadre = vistaPadre
        self.informationLabel = informationLabel
        self.iconoCarga = iconoCarga
    }
    
    func addProduct(producto:InventoryProduct){
        //Create cleaned versions of the data
        self.productName = producto.name.trimmingCharacters(in: .whitespacesAndNewlines)
        self.productImage = producto.image.trimmingCharacters(in: .whitespacesAndNewlines)
        self.productQuantity = producto.quantity
        self.productUnit = producto.unit.trimmingCharacters(in: .whitespacesAndNewlines)
        
        
        let semaphore = DispatchSemaphore(value: 1)
        
        DispatchQueue.global(qos: .default).async {
            semaphore.wait()
            let webScrapingAdapter = WebScrapingAdapter()
            if(self.productImage==""){
                webScrapingAdapter.getInfoFromText(self.productName, semaphore: semaphore)
                DispatchQueue.global(qos: .default).async {
                    semaphore.wait()
                    self.productImage = webScrapingAdapter.information["imageLink"] ?? ""
                    semaphore.signal()
                }
            }else{
                semaphore.signal()
            }
            
            
            DispatchQueue.global(qos: .default).async {
                let db = Firestore.firestore()
                semaphore.wait()
                //Tomar las recetas de la base de datos
                let date = Date()
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd/MM/yyyy hh:mm:ss"
                print(dateFormatter.string(from: date))
                
                db.collection("user").document((UserDefaults.standard.string(forKey: UserDefaultsKeys().getUserIdKey())) ?? "").collection("inventory").addDocument(data: ["name":self.productName,"image":self.productImage,"unit":self.productUnit,"quantity":self.productQuantity, "date":dateFormatter.string(from: date)])
                { err in
                    if let err = err {
                        print("Error writing document: \(err)")
                        self.mostrarInformacionYCerrar("⚠️ Error: Product couldn't be created")
                    } else {
                        print("Document successfully written!")
                        self.mostrarInformacionYCerrar("✅ Product created correctly")
                    }
                    AudioServicesPlayAlertSoundWithCompletion(SystemSoundID(kSystemSoundID_Vibrate)) { }
                }
                semaphore.signal()
            }
            
            
        }
        
    }
    
    func mostrarInformacionYCerrar (_ mensaje:String){
        DispatchQueue.main.async{
            if(self.iconoCarga != nil){
                self.iconoCarga?.isHidden=true
            }
            self.informationLabel!.text=mensaje
        }
        DispatchQueue.global().asyncAfter(deadline: .now()+2, execute: {
            DispatchQueue.main.async{
                //Esta linea mata todas las vistas
                var rootViewController = self.vistaPadre!.view.window!.rootViewController
                rootViewController?.dismiss(animated: true, completion: nil)
                //esta vuelve y lanza el inventario
                let storyboard = UIStoryboard(name: "Inventory" , bundle: nil)
                let inventoryViewController = storyboard.instantiateInitialViewController()
                rootViewController?.present(inventoryViewController!, animated:true, completion:nil)
                
            }
        })
    }
    
    /**
     Funcion que imprime mensajes informativos si la constante imprimirMensaje asi lo indica
     */
    func imprimir(_ mensaje:String){if imprimirMensajes {print("InventoryBrain: "+mensaje)}}
}
