//
//  BarcodeInterpreterAdapter.swift
//  Cookey
//
//  Created by Santiago Triana on 30/10/22.
//

import UIKit
import os.log
import Alamofire
import MobileCoreServices

struct ScannedProduct{
    let name:String
    let description:String
    let encontradoEnCodigoBarras:Bool
    let barcode:String
    
}

protocol BarcodeInterpreterService{
    func postRequest(_ sender:Any, oImage:UIImage, updateProductName:@escaping (String)->Void, updateProductDescription:@escaping (String)->Void, addButton:UIButton, getBarcodeInfo: @escaping (String)->Void)
    func imprimir(_ string:String!)
    func returnMimeType(fileExtension :String)->String
    
}

class BarcodeInterpreterAdapter:BarcodeInterpreterService{
    
    
    let sURLBarcodeReader:String!="https://barcode-reader-cookey.herokuapp.com/readBarcode"
    let imprimirMensajes = true
    
    
    func imprimir(_ string:String!){
        if(imprimirMensajes){
            print("BarcodeInterpreterAdapter: " + string)
        }
    }
    
    struct RequestBodyFormDataKeyValue{
        var sKey:String
        var sValue:String
        var dBlobData: Data
    }
    
    func postRequest(_ sender:Any, oImage:UIImage, updateProductName: @escaping (String)->Void, updateProductDescription: @escaping (String)->Void, addButton: UIButton, getBarcodeInfo: @escaping (String)->Void){
        if(Connectivity.isConnectedToInternet){
            var bodyKeyValue = [RequestBodyFormDataKeyValue]()
            var imageName:String = "image.jpg"
            
            bodyKeyValue.append(RequestBodyFormDataKeyValue(sKey:"id",sValue:"1", dBlobData:Data()))
            let oArrArray = imageName.components(separatedBy: ".") //[0]Name, [1]extension
            let mimeType = self.returnMimeType(fileExtension: oArrArray[1])
            let dData = oImage.jpegData(compressionQuality: 0.3)
            self.imprimir(dData!.description)
            bodyKeyValue.append(RequestBodyFormDataKeyValue(sKey:"img",sValue:imageName, dBlobData:dData!))
            var request = URLRequest(url:URL(string: sURLBarcodeReader)!)
            request.httpMethod=HTTPMethod.post.rawValue
            var rta = ScannedProduct(name: "", description: "", encontradoEnCodigoBarras: false, barcode: "")
            let group = DispatchGroup()
            group.enter()
            AF.upload(multipartFormData: { multipartFormData in
                for formData in bodyKeyValue{
                    if(formData.dBlobData.isEmpty){
                        multipartFormData.append(Data(formData.sValue.utf8), withName: formData.sKey)
                    }
                    else{
                        multipartFormData.append(formData.dBlobData, withName: formData.sKey, fileName: imageName, mimeType: mimeType)
                    }
                }
            }, to: sURLBarcodeReader, method: .post)
            .uploadProgress{ progress in
                self.imprimir(CGFloat(progress.fractionCompleted*100).description)
            }
            .response{
                response in
                if(response.error==nil){
                    var responseString:String! = ""
                    if(response.data != nil){
                        responseString = String(bytes:response.data!, encoding: .utf8)
                    }else{
                        responseString = response.response?.description
                    }
                    self.imprimir("Response - "+responseString)
                    self.imprimir(String(response.response?.statusCode ?? -1))
                    let responseData:NSData! = response.data! as NSData
                    let iDataLength = responseData.length
                    self.imprimir("size: \(iDataLength) bytes")
                    self.imprimir("response time: \(response.metrics?.taskInterval.duration)")
                    let respuestaArray = responseString.components(separatedBy: "\"")
                    //let jsonRespuesta=self.stringToJson(responseString)
                    if(respuestaArray.count==3){
                        self.imprimir("No se ha encontrado un codigo de barras valido")
                        rta = ScannedProduct(name: "", description: "Barcode couldn't be identified", encontradoEnCodigoBarras: false, barcode: "")
                    }else if (respuestaArray.count==5){
                        self.imprimir("Codigo de barras - "+String(respuestaArray[3]))
                        rta = ScannedProduct(name: String(respuestaArray[3]), description: String(respuestaArray[3]), encontradoEnCodigoBarras: true, barcode: String(respuestaArray[3]))
                    }else{
                        self.imprimir("La respuesta del API no fue entendida - "+String(respuestaArray.count))
                    }
                    group.leave()
                }
            }
            self.imprimir("Nombre producto: " + rta.name)
            group.notify(queue: DispatchQueue.main) {
                self.imprimir("Actualizando interfaz con informacion nueva")
                updateProductName(rta.name)
                updateProductDescription(rta.description)
                addButton.isHidden = !rta.encontradoEnCodigoBarras
                getBarcodeInfo(rta.name)
            }
        }else{
            //No hay conectividad
            DispatchQueue.main.async {
                updateProductName("⚠️ Connection Error")
                updateProductDescription("Check your internet connection because something went wrong")
            }
        }
        
        
    }
    
    /**
     retorna el mimetype de un archivo
     */
    func returnMimeType(fileExtension :String)->String{
        if let oUTI = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, fileExtension as NSString, nil)?.takeRetainedValue(){
            if let mimeType = UTTypeCreatePreferredIdentifierForTag(oUTI, kUTTagClassMIMEType, nil)?.takeRetainedValue(){
                return mimeType as String
            }
        }
        return ""
    }
    
}
