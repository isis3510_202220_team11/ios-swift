//
//  HotProductBrain.swift
//  Cookey
//
//  Created by Santiago Triana on 9/11/22.
//

import Foundation
import Firebase

class HotProductBrain{
    let imprimirMensajes = true
    var hotProduct:InventoryProduct = InventoryProduct(id: "", quantity: 1, name: "", unit: "", image: "")
    
    var productos = [InventoryProduct]()
    
    func getHotProduct() -> InventoryProduct{
        return hotProduct
    }
    
    func loadHotProduct(semaphore:DispatchSemaphore){
        if(Connectivity.isConnectedToInternet){
            let db = Firestore.firestore()
            db.collection("hotproduct").getDocuments
            { snapshot, error in
                // Verificacion de errores
                if error == nil
                {
                    // No error
                    // Procede con normalidad
                    if let snapshot = snapshot
                    {
                        
                        // Toma todo el inventario del usuario recetas
                        self.productos = snapshot.documents.map
                        {doc in
                            return InventoryProduct(
                                id: doc.documentID,
                                quantity: doc["quantity"] as? Int ?? 0,
                                name: doc["name"] as? String ?? "",
                                unit: doc["unit"] as? String ?? "",
                                image: doc["image"] as? String ?? "")}
                    }
                    self.hotProduct = self.productos[0]
                    self.imprimir(self.hotProduct.name)
                }
                else
                {
                    // Manejar el error que salga
                    self.imprimir("-----------------------------")
                    self.imprimir("Error en la carga del hotproduct")
                    self.imprimir("-----------------------------")
                }
                semaphore.signal()
                //
            }
        }else{
            semaphore.signal()
        }
    }
    
    func imprimir (_ msje:String){
        if(imprimirMensajes){
            print("HotProductBrain: " + msje)
        }
    }
}
