//
//  WebScrapingAdapter.swift
//  Cookey
//
//  Created by Santiago Triana on 9/11/22.
//

import Foundation
import Alamofire
import MobileCoreServices

protocol WebScrapingService{
    func getInfoFromText (_ text :String,  semaphore:DispatchSemaphore)
    
}

class WebScrapingAdapter:WebScrapingService{
    
    let imprimirMensajes = true
    let sURLInfoProductsAPI:String!="https://barcode-reader-cookey.herokuapp.com/getBarcodeInfo?barcode="
    
    var information:[String:String] = ["":""]
    
    func getInfoFromText(_ text: String, semaphore:DispatchSemaphore) {
        let serializer = DataResponseSerializer(emptyResponseCodes: Set([200,204,205]))
        var urlGet = sURLInfoProductsAPI+text
        urlGet = urlGet.replacingOccurrences(of: " ", with: "")
        imprimir(urlGet)
        let request = URLRequest(url: URL(string:urlGet)!)
        
        AF.request(request)
            .uploadProgress{
            progress in
                
            }
            .response(responseSerializer: serializer){ response in
                if(response.error == nil){
                    var responseString:String!
                    responseString=""
                    if(response.data != nil){
                        responseString =  String(bytes:response.data!, encoding: .utf8)
                    }else{
                        responseString=response.response?.description
                    }
                    self.imprimir(responseString)
                    let jsonResponse = self.stringToJson(responseString)
                    let titleReponse:String = jsonResponse["title"] as! String
                    let imageLink = jsonResponse["imageLink"] as! String
                    self.imprimir(titleReponse+":"+imageLink)
                    self.information = ["title":titleReponse, "imageLink":imageLink]
                    semaphore.signal()
                }
            
                
        }
    }
    
    /**
     convierte un string en un json
     */
    func stringToJson(_ string:String)->Dictionary<String, Any>{
        let emptyDict: Dictionary<String, Any> = [:]
        let data = string.data(using: .utf8)!
        do {
            if let jsonArray = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? Dictionary<String,Any>
            {
               return jsonArray // use the json here
            } else {
                print("BAD JSON")
                return emptyDict
            }
        } catch let error as NSError {
            print(error)
            return emptyDict
        }
    }
    
    func getInfomation()->[String:String]{
        return information
    }
    
    func imprimir(_ string:String!){
        if(imprimirMensajes){
            print("WebScrapingAdapter: " + string)
        }
    }
}
