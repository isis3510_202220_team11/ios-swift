//
//  ConnectivityChecker.swift
//  Cookey
//
//  Created by Santiago Triana on 1/11/22.
//

import Foundation
import Alamofire

struct Connectivity {
  static let sharedInstance = NetworkReachabilityManager()!
  static var isConnectedToInternet:Bool {
      return self.sharedInstance.isReachable
    }
}
