//
//  UserDefaultsKeys.swift
//  Cookey
//
//  Created by Santiago Triana on 4/11/22.
//

import UIKit

struct UserDefaultsKeys{
    private let defaults = UserDefaults.standard
    private let inventoryKey:String = "inventory"
    private let userIdKey:String = "UID"
    private let recipeIdKey:String = "recipe"
    private let promoIdKey:String = "promo"
    private let caloriesIdKey:String = "cal"
    
    /**
     retorna la llave de la promocion actual
     */
    func getPromoKey() -> String{
        var msje:String = promoIdKey + (UserDefaults.standard.string(forKey: promoIdKey) ?? "")
        return msje
    }
    
    /**
     retorna la llave de la receta actual
     */
    func getRecipeKey() -> String{
        var msje:String = recipeIdKey + (UserDefaults.standard.string(forKey: recipeIdKey) ?? "")
        return msje
    }
    
    /**
     retorna la llave de las calorias guardadas
     */
    func getCaloriesKey() -> String{
        var msje:String = caloriesIdKey + (UserDefaults.standard.string(forKey: caloriesIdKey) ?? "")
        return msje
    }
    
    /**
     retorna la llave del inventario del usuario actual
     */
    func getInventoryKey() -> String{
        var msje:String = inventoryKey + (UserDefaults.standard.string(forKey: userIdKey) ?? "")
        return msje
    }
    
    /**
     retorna el UID dado por la base de datos
     */
    func getUserIdKey () -> String{
        return userIdKey
    }

    
    /**
     retorna el userId dado por la db del usuario actual
     */
    func getUserId() -> String{
        var msje:String = (UserDefaults.standard.string(forKey: userIdKey) ?? "")
        return msje
    }
}
